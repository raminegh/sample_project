<?php


namespace App\Http\View\Composers;


use App\Constant;
use App\Settings;
use Illuminate\View\View;

class FooterComposer
{

    public function __construct()
    {

    }

    public function compose(View $view)
    {
        $footerData = \Cache::remember(Constant::CACHE_FOOTER_DATA, Constant::TWELVE_HOUR_SECONDS, function (){
            return unserialize(get_setting(\App\Settings::FOOTER_DATA));
        });
        $socialMedias = $footerData[Settings::FOOTER_SOCIAL_MEDIA] ?? null;
        $colTwo = $footerData[Settings::FOOTER_COL_TWO] ?? null;
        $colThree = $footerData[Settings::FOOTER_COL_THREE] ?? null;
        $signs = $footerData[Settings::FOOTER_SIGNS] ?? null;
        $view->with([
            'socialMedias' => $socialMedias,
            'colTwo' => $colTwo,
            'colThree' => $colThree,
            'signs' => $signs
        ]);
    }
}
