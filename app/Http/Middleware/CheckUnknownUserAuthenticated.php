<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckUnknownUserAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->level == User::UNKNOWN) {
            return $next($request);
        }
        return abort(403);
    }
}
