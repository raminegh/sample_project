<?php

namespace App\Http\Middleware;

use Closure;

class IsImageOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $image = request()->image;
        if(in_array($image->image_id, auth()->user()->images()->pluck('image_id')->toArray())){
            return $next($request);
        }
        abort(404);
    }
}
