<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckNotAdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->level != User::ADMIN) {
            return $next($request);
        }
        return abort(403);
    }
}
