<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckUserAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->level == User::USER) {
            return $next($request);
        }
        return abort(403);
    }
}
