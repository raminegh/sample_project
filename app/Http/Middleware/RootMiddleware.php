<?php

namespace App\Http\Middleware;

use App\Constant;
use Closure;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class RootMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()) {
            $authUser = auth()->user();
            $notificationCount = $authUser->notifications()->where('is_read', 0)->count();
            view()->share([
                'authUser' => $authUser,
                'notificationCount' => $notificationCount,
            ]);
        }

        $isDashboard = Str::contains(Request::route()->getPrefix(), 'dashboard');
        $appGeneralInfo = \Cache::remember(Constant::CACHE_APP_GENERAL_INFO, Constant::ONE_DAY_SECONDS, function (){
            return unserialize(get_setting(\App\Settings::APP_GENERAL_INFO));
        });
        view()->share([
            'appGeneralInfo' => $appGeneralInfo,
            'isDashboard' => $isDashboard
        ]);


        return $next($request);
    }
}
