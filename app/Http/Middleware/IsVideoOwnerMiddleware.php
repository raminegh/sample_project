<?php

namespace App\Http\Middleware;

use Closure;

class IsVideoOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $video = request()->video;
        if(in_array($video->video_id, auth()->user()->videos()->pluck('video_id')->toArray())){
            return $next($request);
        }
        abort(404);
    }
}
