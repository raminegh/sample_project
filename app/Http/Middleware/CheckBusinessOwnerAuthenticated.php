<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckBusinessOwnerAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->level == User::BUSINESS_OWNER) {
            return $next($request);
        }
        return abort(404);
    }
}
