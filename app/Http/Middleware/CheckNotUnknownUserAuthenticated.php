<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckNotUnknownUserAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if(auth()->check() && $user->level != User::UNKNOWN) {
            if (!$user->username) {
                if ($user->level == User::ADMIN)
                    return redirect()->route('admin.profile.index')->with('error', "باید برای حسابتان، نام کاربری انتخاب کنید");
                return redirect()->route('app.user.userInfo')->with('error', "برای استفاده از حساب خود، تعیین نام کاربری الزامی است");
            } else if (!$user->password) {
                if ($user->level == User::ADMIN)
                    return redirect()->route('admin.profile.resetPassword')->with('error', "برای استفاده از حساب خود، تعیین رمز عبور الزامی است");
                return redirect()->route('app.user.userInfo')->with('error', "برای استفاده از حساب خود، تعیین نام کاربری الزامی است");
            }
            return $next($request);
        }

        return redirect()->route('app.user.userInfo')->with('error', "شما باید در ابتدا اطلاعات خود را تکمیل کنید تا بتوانید به امکانات سایت دسترسی پیدا کنید.");
    }
}
