<?php

namespace App\Http\Requests\App\Video;

use Illuminate\Foundation\Http\FormRequest;

class SetInfoVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(request()->hasFile('subtitle_fa')) {
            return request()->subtitle_fa->getClientOriginalExtension() !== 'vtt' ? false : true ;
        }

        if(request()->hasFile('subtitle_en')) {
            return request()->subtitle_en->getClientOriginalExtension() !== 'vtt' ? false : true ;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'video_id' => 'required|string|min:5|max:5|exists:videos,video_id',
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'cover' => 'nullable|mimes:jpeg,png,jpg|max:4096',
            'category_id' => 'required|integer|exists:categories,id',
            'videoable_id' => 'required|integer|min:1|exists:albums,id',
            'is_downloadable' => 'nullable|in:0,1',
            'subtitle_fa' => 'nullable|max:1024',
            'subtitle_en' => 'nullable|max:1024',
            'watermark' => 'nullable|in:0,1',
            'tags' => 'nullable|array',
            'tags.*' => 'string',
        ];
    }
}
