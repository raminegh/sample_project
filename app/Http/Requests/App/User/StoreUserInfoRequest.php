<?php

namespace App\Http\Requests\App\User;

use App\Constant;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $invalidUsernames = Constant::INVALID_USERNAMES;

        $invalidUsernamesStr = implode(",", $invalidUsernames);

        $rules = [
            'name' => "required|min:3|max:64|string",
            'bio' => "nullable|min:3|max:235|string",
            'website' => "nullable|string",
            'instagram' => "nullable|string",
            'avatar' => "nullable|image",
        ];

        request()->username == auth()->user()->username ?
            $rules['username'] = "required|min:3|max:64|alpha_num|not_in:" . $invalidUsernamesStr:
            $rules['username'] = "required|min:3|max:64|alpha_num|unique:users,username|not_in:" . $invalidUsernamesStr;
        if(auth()->user()->level === User::UNKNOWN) {
            $rules['level'] = "required|in:2,3,4";
            $rules['password'] = "required|min:6|max:32|string";
        }

        return $rules;
    }

}
