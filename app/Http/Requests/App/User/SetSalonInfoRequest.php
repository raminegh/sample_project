<?php

namespace App\Http\Requests\App\User;

use Illuminate\Foundation\Http\FormRequest;

class SetSalonInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_id' => 'required|integer|exists:businesses,id',
            'services_id' => 'required|array',
            'services_id.*' => 'integer|min:1|exists:services,id',
            'name' => 'required|string|min:2|max:64',
            'description' => 'required|string|min:8|max:330',
            'address' => 'nullable|string|min:8|max:255',
            'province_id' => 'required|int|min:1|exists:provinces,id',
            'city_id' => 'required|int|min:1|exists:cities,id',
            'lat_map' => 'nullable|numeric',
            'lng_map' => 'nullable|numeric',
            'manager' => 'nullable|string|min:3|max:64',
            'tel' => 'nullable|string|min:4|max:64'
        ];
    }
}
