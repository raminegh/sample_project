<?php

namespace App\Http\Requests\App\User;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class SetIsEmployRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->level === User::BEAUTY_ACTIVIST ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is-employ' => 'required|in:0,1'
        ];
    }
}
