<?php

namespace App\Http\Requests\App\Comment;

use App\Constant;
use Illuminate\Foundation\Http\FormRequest;

class StoreCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'comment' => 'required|string|min:2|max:512',
            'parent_id' => 'nullable|integer|min:0',
            'commentable_id' => 'required|integer|min:1',
            'commentable_type' => 'required|string|in:' . implode(',', Constant::MODEL_TYPE_ARRAY),
        ];

        if(!auth()->check()) {
            $rules['email'] = 'nullable|email';
            $rules['name'] = 'nullable|string|min:2|max:64';
        }

        return $rules;
    }

}
