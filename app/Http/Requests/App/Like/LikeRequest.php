<?php

namespace App\Http\Requests\App\Like;

use App\Constant;
use Illuminate\Foundation\Http\FormRequest;

class LikeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'action' => 'required|string|in:like,dislike',
            'likeable_id' => 'required|integer|min:1',
            'likeable_type' => 'required|string|in:' . implode(',', Constant::MODEL_TYPE_ARRAY),
        ];
    }
}
