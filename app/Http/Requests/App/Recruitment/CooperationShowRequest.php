<?php

namespace App\Http\Requests\App\Recruitment;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class CooperationShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(request()->cooperation->receiver_id === auth()->id()) {
            return true;
        }
        abort(403);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
