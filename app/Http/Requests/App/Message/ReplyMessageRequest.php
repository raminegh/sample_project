<?php

namespace App\Http\Requests\App\Message;

use Illuminate\Foundation\Http\FormRequest;

class ReplyMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(in_array(request()->message_id, auth()->user()->receivedMessages()->pluck('id')->toArray())){
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message_id' => 'required|int|min:1|exists:messages,id',
            'text' => 'required|string|min:3'
        ];
    }
}
