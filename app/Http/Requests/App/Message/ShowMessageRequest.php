<?php

namespace App\Http\Requests\App\Message;

use Illuminate\Foundation\Http\FormRequest;

class ShowMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(in_array(request()->message->id, auth()->user()->messages()->pluck('id')->toArray()) || in_array(request()->message->id, auth()->user()->receivedMessages()->pluck('id')->toArray())) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
