<?php

namespace App\Http\Requests\Image;

use Illuminate\Foundation\Http\FormRequest;

class UploadImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|string|exists:images,image_id',
            'title' => 'required|string|max:255',
            'description' => 'nullable|string|max:1024',
            'imageable_id' => 'required|integer|min:1|exists:albums,id',
            'tags' => 'nullable|array',
            'tags.*' => 'string',
            'category_id' => 'required|integer|exists:categories,id',
        ];
    }
}
