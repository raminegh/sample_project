<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
					'id' => 'numeric|exists:users,id',
					'first_name' => "required|string|min:2",
					'last_name' => "required|string|min:2",
					'username' => "nullable|string|min:2",
        ];
    }
}
