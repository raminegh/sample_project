<?php

namespace App\Http\Requests\Admin;

use App\Models\Video;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class ListRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'nullable|integer|min:1',
            'sort' => 'nullable|string',
            'type' => 'nullable|in:desc,asc',
            'start_date' => 'nullable|string',
            'end_date' => 'nullable|string',
            'keyword' => 'nullable|string',
            'user_id' => 'nullable|integer|min:1',
            'state' => 'nullable|string|in:' . implode(",", Video::STATE_ARRAY),
            'is_approved' => 'nullable|string|in:zero,one',
            'is_active' => 'nullable|string|in:zero,one'
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */

    protected function prepareForValidation()
    {
        $this->merge([
            'start_date' => convertJalaliToGregorian($this->start_date),
            'end_date' => convertJalaliToGregorian($this->end_date)
        ]);
    }


    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }


}
