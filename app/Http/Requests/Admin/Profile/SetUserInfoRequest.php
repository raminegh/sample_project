<?php

namespace App\Http\Requests\Admin\Profile;

use App\Constant;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class SetUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $invalidUsernames = Constant::INVALID_USERNAMES;
        $invalidUsernamesStr = implode(",", $invalidUsernames);

        $rules = [
            'name' => "required|min:3|max:64|string",
            'bio' => "nullable|min:3|max:235|string",
            'website' => "nullable|string",
            'instagram' => "nullable|string",
        ];

        request()->username == auth()->user()->username ?
            $rules['username'] = "required|min:3|max:64|alpha_num|not_in:" . $invalidUsernamesStr:
            $rules['username'] = "required|min:3|max:64|alpha_num|unique:users,username|not_in:" . $invalidUsernamesStr;

        return $rules;
    }
}
