<?php

namespace App\Http\Requests\Admin\Ads;

use App\models\Ads;
use Illuminate\Foundation\Http\FormRequest;

class AdStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'start_at' => convertJalaliToGregorian($this->start_at),
            'expire_at' => convertJalaliToGregorian($this->expire_at)
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
//            'category_id' => 'required|int|min:0|exists:categories,id',
            'start_at' => 'required|date|after_or_equal:created_at',
            'expire_at' => 'required|date|after_or_equal:created_at',
            'description' => 'nullable|string|min:3',
            'title' => 'nullable|string|min:1',
            'text' => 'nullable|reportage|string',
            'image' => 'nullable|file|image',
            'teaser' => 'nullable|string',
            'reportage' => 'nullable|text|url',
            'related_link' => 'nullable|string',
        ];

        if (request()->route() != route('admin.ads.create')) {
            return $rules;
        }

        $rules['user_id'] = 'required|int|min:0|exists:users,id';
        $rules['type'] = 'required|int|min:1|in:' . implode(",", Ads::AD_TYPE_ARRAY);
        $rules['text'] = 'required_without_all:image,teaser,reportage|string';
        $rules['image'] = 'required_without_all:text,teaser,reportage|file|image';
        $rules['teaser'] = 'required_without_all:text,image,reportage|exists:ads,teaser';
        $rules['reportage'] = 'required_without_all:text,image,teaser|url';
        return $rules;
    }
}
