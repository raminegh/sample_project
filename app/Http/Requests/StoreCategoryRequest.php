<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => "required|string|min:2",
			'label' => "nullable|string|min:2",
			'parent_id' => 'nullable|numeric|exists:categories,id',
            'type' => 'required|integer|in:1,2,3'
		];
	}
}
