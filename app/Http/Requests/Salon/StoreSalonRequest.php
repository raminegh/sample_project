<?php

namespace App\Http\Requests\Salon;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreSalonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        //return auth()->user()->level === User::BUSINESS_OWNER ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_id' => 'required|integer|min:1|exists:businesses,id',
            'province_id' => 'nullable|integer|min:1|exists:provinces,id',
            'city_id' => 'nullable|integer|min:1|exists:citeis,id',
            'name' => 'nullable|string|min:1',
            'description' => 'nullable|string|min:8',
            'address' => 'nullable|string|min:4|max:191',
            'manager' => 'nullable|string|min:2',
            'tel' => 'nullable|string|min:4',
            'google_map_location' => 'nullable|string|min:10',
            'services' => 'required|array',
            'services.*' => 'required|integer|min:1|exists:services,id'
        ];
    }
}
