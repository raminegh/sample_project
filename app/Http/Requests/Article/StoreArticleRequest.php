<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class StoreArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */

    protected function prepareForValidation()
    {
        $this->merge([
            'publish_at' => convertJalaliToGregorian($this->publish_at),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => "required|string|min:2|max:256",
            'description' => "nullable|string|min:2|max:256",
            'body' => "required",
            'cover' => "required|file|mimes:jpeg,jpg,png,gif,svg|max:2048",
            'categories' => 'required|array',
            'categories.*' => 'numeric|exists:categories,id',
            'tags' => 'nullable|array',
            'tags.*' => 'string',
            'publish_at' => 'required|date|after_or_equal:created_at'
        ];
        if (request()->route() != route('admin.article.create'))
            $rules['cover'] = "nullable|mimes:jpeg,jpg,png,gif,svg|max:2048";

        return $rules;
    }
}
