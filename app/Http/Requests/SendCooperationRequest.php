<?php

namespace App\Http\Requests;

use App\Http\Controllers\Web\CooperationController;
use App\Models\Cooperation;
use Illuminate\Foundation\Http\FormRequest;

class SendCooperationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type_id' => 'required|int|min:1',
            'type' => 'required|in:' . implode(',', Cooperation::COOPERATION_REQUEST_TO_ARRAY),
            'description' => 'nullable|string',
        ];

        switch (request()->type){
            case Cooperation::COOPERATION_REQUEST_TO_USER:
                $rules['type_id'] = 'required|int|min:1|exists:users,id';
                break;
            case Cooperation::COOPERATION_REQUEST_TO_SALON:
                $rules['type_id'] = 'required|int|min:1|exists:salons,id';
                break;
            case Cooperation::COOPERATION_REQUEST_TO_EMPLOYMENT:
                $rules['type_id'] = 'required|int|min:1|exists:employments,id';
                break;
        }

        return $rules;
    }
}
