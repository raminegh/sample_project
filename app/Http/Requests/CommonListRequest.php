<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommonListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'nullable|int|min:1',
            'category' => 'nullable|int|min:1|exists:categories,id',
            'tag' => 'nullable|string|exists:tags,name',
            'q' => 'nullable|string|max:64',
        ];
    }
}
