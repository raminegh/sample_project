<?php

namespace App\Http\Requests\Access;

use Illuminate\Foundation\Http\FormRequest;

class StoreAccessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'roles' => 'required|array',
            'roles.*' => 'integer|min:0|not_in:0|exists:roles,id',
        ];
        if(request()->url() == route('admin.access.store')) {
            $validations['user'] = 'required|min:0|not_in:0|exists:users,id';
            return $validations;
        }

        return $validations;

    }
}
