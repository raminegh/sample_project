<?php

namespace App\Http\Resources\Api\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->user_id,
            'bio' => $this->bio,
            'instagram' => $this->instagram,
            'website' => $this->website,
            'place_work' => $this->place_work,
            'cover' => $this->cover,
            'video_intro' => $this->video_intro,
            'cv' => $this->cv,
        ];
    }
}
