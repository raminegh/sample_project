<?php

namespace App\Http\Resources\Api\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            'avatar' => $this->avatar || getAvatar($this),
            'level' => $this->level,
            'is_employed' => $this->is_employed,
            'followers_count' => $this->followers_count,
            'followings_count' => $this->followings_count,
            'views_count' => $this->views_count
        ];
    }
}
