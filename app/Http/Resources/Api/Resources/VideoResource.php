<?php

namespace App\Http\Resources\Api\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'video_id' => $this->video_id,
            'user_id' => $this->user_id,
            'hls_url' => $this->hls_url,
            'download_url' => $this->download_url,
            'title' => $this->title,
            'description' => $this->description,
            'view_count' => $this->view_count,
            'comment_count' => $this->comment_count,
            'like_count' => $this->like_count,
            'cover' => $this->cover,
            'subtitles' => $this->subtitles,
            'duration' => $this->duration,
            'publish_at' => $this->publish_at,
            'is_downloadable' => $this->is_downloadable,
            'videoable_id' => $this->videoable_id,
            'videoable_type' => $this->videoable_type,
        ];
    }
}
