<?php

namespace App\Http\Resources\Api\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'business_id' => $this->business_id,
            'user_id' => $this->user_id,
            'province_id' => $this->province_id,
            'city_id' => $this->city_id,
            'name' => $this->name,
            'manager' => $this->manager,
            'description' => $this->description,
            'tel' => $this->tel,
            'address' => $this->address,
            'lat' => $this->lat,
            'lng' => $this->lng,
        ];
    }
}
