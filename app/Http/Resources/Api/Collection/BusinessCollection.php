<?php

namespace App\Http\Resources\Api\Collection;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BusinessCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item) {
           return [
               'id' => $item->id,
               'name' => $item->name,
               'description' => $item->description
           ];
        });
    }

    public function with($request)
    {
        return [
            'status' => true
        ];
    }
}
