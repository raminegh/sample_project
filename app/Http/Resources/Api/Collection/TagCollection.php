<?php

namespace App\Http\Resources\Api\Collection;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TagCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item) {
            return [
                'value' => $item->id,
                'label' => $item->name
            ];
        });
    }

    public function with($request)
    {
        return [
            'status' => true
        ];
    }

}
