<?php

namespace App\Http\Resources\Api\Collection;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NewsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item){
            return [
                'id' => $item->id,
                'title' => $item->title,
                'description' => $item->description,
                'slug' => $item->slug,
                'cover' => $item->cover,
                'view_count' => $item->view_count,
                'comment_count' => $item->comment_count,
                'like_count' => $item->like_count,
                'created_at' => $item->created_at,
            ];
        });
    }

    public function with($request)
    {
        return [
            'status' => true
        ];
    }
}
