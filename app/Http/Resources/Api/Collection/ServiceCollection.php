<?php

namespace App\Http\Resources\Api\Collection;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ServiceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item){
            return [
                'id' => $item->id,
                'business_id' => $item->business_id,
                'name' => $item->name,
                'description' => $item->description
            ];
        });
    }

    public function with($request)
    {
        return [
            'status' => true
        ];
    }
}
