<?php


namespace App\Http\Services;

use App\Constant;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class ImageService
{
    public static function ajaxList(Request $request, string $username) {
        $user = User::Username($username);
        $page = $request->page ?? 1;
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $items = $user->approvedImages()->paginate(Constant::APP_LIST_COUNT);
        return view('app.image.list', compact('items'));
    }
}
