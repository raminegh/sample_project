<?php


namespace App\Http\Services\App;

use App\Article;
use App\Category;
use App\Constant;
use App\Events\VisitPostEvent;
use App\models\Ads;
use App\Models\Discount;
use App\Models\Employment;
use App\Models\Image;
use App\Models\Salon;
use App\Models\Service;
use App\Models\Video;
use App\News;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class AppCommonService
{

    public static function uploadImage(Request $request, $folder = 'unknown', $ratio = null) {
        try {
            $imageUrl = saveImage($request->file('image'), $folder);
            return response()->json([
                'status' => true,
                'url' => $imageUrl
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => "آپلود با خطا مواجه شده است"
            ]);
        }
    }

    public static function uploadVideo(Request $request, $folder = 'unknown') {
        try {
            $videoUrl = uploadVideo($request->file('video'), $folder);
            return response(['video' => $videoUrl], 200);
        } catch (Exception $e) {
            return response(['message' => 'خطایی رخ داده است'], 500);
        }
    }

    public static function ajaxList(Request $request, string $type)
    {
        $page = $request->page ?? 1;
        $category = $request->category;
        $tag = $request->tag;
        $q = $request->q;

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $items = null;
        switch ($type) {
            case Constant::ARTICLE_TYPE:
                if ($tag) {
                    $items = Tag::whereName($tag)->first()->articles();
                } else if ($category) {
                    $items = Category::find($category)->articles();
                } else if ($q){
                    $items = Article::search($q)->latest();
                } else {
                    $items = Article::orderBy('id', 'DESC');
                }
                break;
            case Constant::NEWS_TYPE:
                if ($tag) {
                    $items = Tag::whereName($tag)->first()->news();
                } else if ($category) {
                    $items = Category::find($category)->news();
                } else if ($q){
                    $items = News::search($q)->latest();
                } else {
                    $items = News::orderBy('id', 'DESC');
                }
                break;
            case Constant::EMPLOYMENT_TYPE:
                if ($tag) {
                    $items = Tag::whereName($tag)
                        ->first()
                        ->employments();
                } else if ($category) {
                    $items = Category::find($category)
                        ->employments();
                } else if ($q){
                    $items = Employment::search($q);
                } else {
                    $items = Employment::getAll();
                }
                break;
            case Constant::DISCOUNT_TYPE:
                if ($tag) {
                    $items = Tag::whereName($tag)
                        ->first()
                        ->discounts();
                    $items = Discount::applyShowCondition($items);
                } else if ($category) {
                    $items = Category::find($category)
                        ->discounts();
                    $items = Discount::applyShowCondition($items);
                } else if ($q){
                    $items = Discount::search($q);
                } else {
                    $items = Discount::GetAll()->latest();
                }
                break;
            case Constant::VIDEO_TYPE:
                if ($tag) {
                    $items = Tag::whereName($tag)
                        ->first()
                        ->videos();
                } else if ($category) {
                    $items = Category::find($category)
                        ->videos();
                } else if ($q){
                    $items = Video::search($q);
                } else {
                    $items = Video::GetAll();
                }
                break;
            case Constant::IMAGE_TYPE:
                if ($tag) {
                    $items = Tag::whereName($tag)
                        ->first()
                        ->images();
                } else if ($category) {
                    $items = Category::find($category)
                        ->images();
                } else if ($q){
                    $items = Image::search($q);
                } else {
                    $items = Image::GetAll();
                }
                break;
            case Constant::USER_TYPE:
                if ($q){
                    $items = User::Search($q);
                } else {
                    $items = User::GetAll();
                }
                break;
            case Constant::SALON_TYPE:
                $serviceId = $request->serviceId;
                if ($category) {
                    $items = Category::find($category)
                        ->salons();
                } else if ($q){
                    $items = Salon::search($q);
                } else if ($serviceId) {
                    $service = Service::findOrFail($serviceId);
                    $items = $service->salons()->with('user');
                } else {
                    $items = Salon::getAll();
                }
                break;
        }


        $items = $items->paginate(12);
        return view('app.' . $type . '.list', ['items' => $items]);
    }

    public static function showArticle($item, $type = Constant::ARTICLE_TYPE)
    {
        $comments = $item->approvedComments()->get();
        $isLike = in_array(auth()->id(), $item->likes()->pluck('user_id')->toArray());
        event(new VisitPostEvent($item));
        $relateds = $item->relateds();
        $ads = Ads::Ads('image')->get();
        $tags = $item->tags;
        return view('app.article-new.show', compact('item', 'tags', 'comments', 'relateds', 'ads'))->with(['isLike' => $isLike, 'type' => $type]);
    }
}


