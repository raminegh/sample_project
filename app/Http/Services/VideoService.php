<?php


namespace App\Http\Services;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class VideoService
{
    public static function ajaxList(Request $request, string $username) {
        $user = User::Username($username);
        $page = $request->page ?? 1;
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $items = $user->approvedVideos()->paginate(12);
        return view('app.video.list', compact('items'));
    }
}
