<?php
namespace App\Http\Services;

use App\Category;
use App\Constant;

class CommonService
{
    public static function getParentsCategories($type = Category::TYEP_ARTICLE)
    {
        switch ($type) {
            case Category::TYEP_ARTICLE:
                return self::fetchParentsCategories($type, Constant::CACHE_CATEGORIES_ARTICLE);
            case Category::TYEP_NEWS:
                return self::fetchParentsCategories($type, Constant::CACHE_CATEGORIES_NEWS);
            case Category::TYEP_MEDIA:
            default:
                return self::fetchParentsCategories($type, Constant::CACHE_CATEGORIES_MEDIA);
        }
    }

    public static function getCategories($type = Category::TYEP_ARTICLE)
    {
        switch ($type) {
            case Category::TYEP_ARTICLE:
                return self::fetchCategories($type, Constant::CACHE_CATEGORIES_ARTICLE);
            case Category::TYEP_NEWS:
                return self::fetchCategories($type, Constant::CACHE_CATEGORIES_NEWS);
            case Category::TYEP_MEDIA:
            default:
                return self::fetchCategories($type, Constant::CACHE_CATEGORIES_MEDIA);
        }
    }

    public static function delete($model)
    {
        $model->categories()->detach();
        $model->tags()->detach();
        $model->comments()->delete();
        $model->likes()->delete();
        $model->views()->delete();
        $model->delete();
        return toJson(['status' => true, 'message' => "آیتم موردنظر  با موفقیت حذف شد."]);
    }



    private static function fetchParentsCategories($type, $keyCache) {
        return \Cache::remember($keyCache, Constant::TWO_HOUR_SECONDS, function () use ($type) {
            return Category::whereNull('parent_id')->whereType($type)->get();
        });
    }

    private static function fetchCategories($type, $keyCache) {
        return \Cache::remember($keyCache, Constant::TWO_HOUR_SECONDS, function () use ($type) {
            return Category::whereType($type)->get();
        });
    }

}


