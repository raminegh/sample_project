<?php

namespace App\Http\Services\Admin;

use App\Category;
use App\Constant;
use App\Http\Controllers\Admin\ArticleController;
use App\Http\Services\CommonService;
use App\Tag;
use Illuminate\Http\Request;

class AdminCommonService
{
    public static function filter($query, Request $request)
    {
        if ($request->user_id)
            $query->where('user_id', $request->user_id);

        if ($request->type_date)
            if ($request->start_date || $request->end_date)
                $query->whereBetween($request->type_date, [$request->start_date ?? now(), $request->end_date ?? now()]);

        if ($request->is_active) {
            $isActive = $request->is_active === 'one' ? 1 : 0;
            $query->where('is_active', $isActive);
        }

        if ($request->is_approved) {
            $isApproved = $request->is_approved === 'one' ? 1 : 0;
            $query->where('is_approved', $isApproved);
        }

        if ($request->level)
            $query->where('level', $request->level);

        if ($request->title)
            $query->where('title', 'LIKE', '%' . $request->title . '%');

        if ($request->keyword)
            $query->where('name', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('phone', 'LIKE', '%' . $request->keyword . '%');

        return $query;
    }

    public static function disapproval($model, Request $request = null)
    {
        $model->disapproval_description = $request->disapproval_description;
        $model->is_approved = 0;
        $model->save();
        return jsonResponseTrue();
    }

    public static function approve($model, Request $request = null)
    {
        $model->is_approved = 1;
        $model->save();
        return jsonResponseTrue();
    }

    public static function createArticle($type = null)
    {
        $categories = CommonService::getCategories($type === Constant::NEWS_TYPE ? Category::TYEP_NEWS : Category::TYEP_ARTICLE);
        return view('admin.article-news.create', compact('categories'))
            ->with(['type' => $type, 'title' => $type ? 'ایجاد خبر جدید' : "ایجاد مقاله جدید"]);
    }

    public static function storeArticle(Request $request, $type = Constant::ARTICLE_TYPE)
    {
        $video = null;
        if ($request->hasFile('video'))
            $video = uploadVideo($request->file('video'), $type);


        $cover = $request->file('cover');
        $cover = coverProduction($cover, Constant::ARTICLE_TYPE, ArticleController::ARTICLE_COVER_IMAGE_WIDTH, ArticleController::ARTICLE_COVER_IMAGE_HEIGHT);
        $createData = [
            'title' => $request->title,
            'description' => $request->description,
            'body' => $request->body,
            'cover' => $cover,
            'video' => $video,
            'video_link' => $request->video_link,
            'publish_at' => $request->publish_at
        ];
        $model = null;
        if ($type === Constant::NEWS_TYPE) {
            $model = auth()->user()->news()->create($createData);
            \Cache::forget(Constant::CACHE_NEWS_HOME_PAGE);
        } else {
            $model = auth()->user()->articles()->create($createData);
            \Cache::forget(Constant::CACHE_ARTICLES_HOME_PAGE);
        }

        setCategories($request->categories, $model);
        setTags($request->tags, $model);

        return redirect()->route('admin.' . $type . '.index')->with(['success' => ($type === Constant::NEWS_TYPE ? 'خبر' : 'مقاله') . " با موفقیت ایجاد شد"]);
    }

    public static function editArticle($item, $type = null)
    {
        $categories = CommonService::getCategories($type === Constant::NEWS_TYPE ? Category::TYEP_NEWS : Category::TYEP_ARTICLE);
        $tags = $item->tags;
        $categoriesSelected = $item->categories->pluck('id')->toArray();
        return view('admin.article-news.edit', compact('item', 'categories', 'categoriesSelected', 'tags'))
            ->with(['type' => $type, 'title' => $type ? 'ویرایش خبر' : 'ویرایش مقاله']);
    }

    public static function updateArticle(Request $request, $model, $type = 'article')
    {
        $cover = $request->file('cover');

        if ($cover)
            $model->cover = coverProduction($cover, Constant::ARTICLE_TYPE, ArticleController::ARTICLE_COVER_IMAGE_WIDTH, ArticleController::ARTICLE_COVER_IMAGE_HEIGHT);

        if ($request->hasFile('video')) {
            $model->video = uploadVideo($request->file('video'), $type);
        } else if ($request->video_link) {
            $model->video_link = $request->video_link;
        }

        setCategories($request->categories, $model);
        setTags($request->tags, $model);

        $model->update([
            "title" => $request->title,
            "description" => $request->description,
            "body" => $request->body,
            "publish_at" => $request->publish_at,
        ]);

        if ($type === Constant::NEWS_TYPE) {
            \Cache::forget(Constant::CACHE_ARTICLES_HOME_PAGE);
        } else {
            \Cache::forget(Constant::CACHE_NEWS_HOME_PAGE);
        }

        return redirect()->route('admin.' . $type . '.index')->with(['success' => ($type === Constant::NEWS_TYPE ? 'خبر' : 'مقاله') . " با موفقیت ویرایش شد"]);
    }

    public static function active($model)
    {
        $model->is_active = 1;
        $model->save();
        return jsonResponseTrue();
    }

    public static function deactive($model)
    {
        $model->is_active = 0;
        $model->save();
        return jsonResponseTrue();
    }
}


