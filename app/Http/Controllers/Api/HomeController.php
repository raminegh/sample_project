<?php

namespace App\Http\Controllers\Api;

use App\Article;
use App\Constant;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Collection\ArticleCollection;
use App\Http\Resources\Api\Collection\NewsCollection;
use App\Http\Resources\Api\Collection\ServiceCollection;
use App\Models\Service;
use App\Models\TopSalon;
use App\News;
use App\Settings;

class HomeController extends Controller
{
    public function index()
    {
        \Cache::forget(Constant::CACHE_MAIN_SLIDES);
        \Cache::forget(Constant::CACHE_TOP_SALONS);
        \Cache::forget(Constant::CACHE_ARTICLES_HOME_PAGE);
        \Cache::forget(Constant::CACHE_NEWS_HOME_PAGE);
        \Cache::forget(Constant::CACHE_SERVICES_HOME_PAGE);
        $slides = \Cache::remember(Constant::CACHE_MAIN_SLIDES, Constant::TWO_HOUR_SECONDS, function () {
            return unserialize(get_setting(Settings::SLIDE_MAIN_PAGE));
        });

        $topSalons = \Cache::remember(Constant::CACHE_TOP_SALONS, Constant::TWO_HOUR_SECONDS, function (){
            return TopSalon::orderBy('priority', 'desc')->with('user:id,username,avatar', 'salon:user_id,name,description')->get();
        });

        $articles = \Cache::remember(Constant::CACHE_ARTICLES_HOME_PAGE, Constant::TWO_HOUR_SECONDS, function (){
            $articles = Article::orderBy('id', 'desc')->take(8)->get();
            return new ArticleCollection($articles);
        });

        $news = \Cache::remember(Constant::CACHE_NEWS_HOME_PAGE, Constant::THIRTY_MINUTE_SECONDS, function (){
            $news = News::orderBy('id', 'desc')->take(8)->get();
            return new NewsCollection($news);
        });

        $services = \Cache::remember(Constant::CACHE_SERVICES_HOME_PAGE, Constant::TWO_HOUR_SECONDS, function (){
            $services = Service::all();
            if($services->count() > 12){
                $services = $services->random(floor(($services->count() / 2)) + 1);
                return new ServiceCollection($services);
            }
            return new ServiceCollection($services);
        });

        return convertToJson([
            'slides' => $slides,
            'topSalons' => $topSalons,
            'articles' => $articles,
            'news' => $news,
            'services' => $services
        ]);

    }
}

