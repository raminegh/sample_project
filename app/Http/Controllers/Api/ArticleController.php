<?php

namespace App\Http\Controllers\Api;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Collection\ArticleCollection;
use App\Http\Resources\Api\Collection\CategoryCollection;
use App\Http\Resources\Api\Resources\ArticleResource;
use App\Http\Resources\Api\Resources\PaginationResource;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function getArticles()
    {
        $articles = Article::paginate(8);
        $categories = Category::all();
        return convertToJson([
            'articles' => new ArticleCollection($articles),
            'pagination' => new PaginationResource($articles),
            'categories' => new CategoryCollection($categories),
        ]);
    }

    public function getArticle($slug)
    {
        $article = Article::where('slug', $slug)->firstOrFail();
        $article->increment('view_count'); //TODO: after better this
        $comments = $article->comments()->where('approved', 1)->where('parent_id', 0)->latest()->with(['comments' => function($query) {
            $query->where('approved', 1)->oldest();
        }])->get();
        return new ArticleResource($article, $comments);
    }

}
