<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Collection\NewsCollection;
use App\Http\Resources\Api\Resources\NewsResource;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function getNews()
    {
        $news = News::all();
        return New NewsCollection($news);
    }

    public function getNew($slug)
    {
        $new = News::where('slug', $slug)->firstOrFail();
        $new->increment('view_count'); //TODO: after better this
        $comments = $new->comments()->where('approved', 1)->where('parent_id', 0)->latest()->with(['comments' => function($query) {
            $query->where('approved', 1)->oldest();
        }])->get();
        return new NewsResource($new, $comments);
    }
}
