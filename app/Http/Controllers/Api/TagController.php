<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Collection\TagCollection;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function getList()
    {
        return new TagCollection(Tag::all());
    }
}
