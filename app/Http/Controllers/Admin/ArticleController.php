<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Constant;
use App\Http\Requests\Admin\ListRequest;
use App\Http\Requests\Article\StoreArticleRequest;
use App\Http\Services\Admin\AdminCommonService;
use App\Http\Services\CommonService;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class ArticleController extends Controller
{
	const ARTICLE_COVER_IMAGE_WIDTH = 370;
	const ARTICLE_COVER_IMAGE_HEIGHT = 250;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		return view('admin.article-news.index')->with(['title' => 'لیست مقالات']);
	}

	public function list(ListRequest $request)
	{
		$page = $request->page ?? 1;
		$sort = $request->sort ?? 'id';
		$type = $request->type ?? 'desc';

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        $items = Article::orderBy($sort, $type)->with('user');
        $items = AdminCommonService::filter($items, $request);
        $counts = $items->count();
        $items = $items->paginate(Constant::ADMIN_LIST_COUNT);
        return view('admin.article-news.articles-table', compact('items', 'counts', 'page'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
	    return AdminCommonService::createArticle();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(StoreArticleRequest $request)
	{
	    return AdminCommonService::storeArticle($request);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Article  $article
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit(Article $article)
	{
	    return AdminCommonService::editArticle($article);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Article  $article
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(StoreArticleRequest $request, Article $article)
	{
	    return AdminCommonService::updateArticle($request, $article);
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     */

	public function delete(Article $article)
	{
        \Cache::forget(Constant::CACHE_ARTICLES_HOME_PAGE);
        return CommonService::delete($article);
    }


    public function disapproval(Article $article)
    {
        return AdminCommonService::deactive($article);
    }

    public function accept(Article $article)
    {
        return AdminCommonService::active($article);
    }

}
