<?php

namespace App\Http\Controllers\Admin;

use App\Constant;
use App\Http\Requests\Admin\DeleteRequest;
use App\Http\Services\Admin\AdminCommonService;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class TagController extends Controller
{
    public function index()
    {
        $tags = Tag::all();
        return view('admin.tag.index', compact('tags'));
    }


    public function list(Request $request)
    {
        $page = $request->input('page');

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $tags = Tag::orderBy('id', 'DESC');
        $tags = $tags->paginate(20);
        return view('admin.tag.tags-table', compact('tags'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|string',
        ]);
        $result = Tag::create([
            'name' => $request->input('name'),
        ]);
        if(!$result) {
            return response()->json(['status' => false, 'message' => "در ایجاد دسته بندی جدید مشکلی بوجود آمد"]);
        }
        return response()->json(['status' => true, 'message' => "دسته بندی جدید با موفقیت ایجاد شد."]);
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|string',
        ]);
        $tag = Tag::findOrFail($request->input('id'));
        $result = $tag->update([
            'name' => $request->input('name'),
        ]);
        if(!$result) {
            return response()->json(['status' => false, 'message' => "در ایجاد دسته بندی جدید مشکلی بوجود آمد"]);
        }
        return response()->json(['status' => true, 'message' => "دسته بندی جدید با موفقیت ایجاد شد."]);
    }


    public function delete(Tag $tag)
    {
        return AdminCommonService::delete($tag);
    }
}
