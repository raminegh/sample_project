<?php

namespace App\Http\Controllers\Admin;

use App\Constant;
use App\Http\Requests\Admin\ListRequest;
use App\Http\Requests\Article\StoreArticleRequest;
use App\Http\Services\Admin\AdminCommonService;
use App\Http\Services\CommonService;
use App\News;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class NewsController extends Controller
{
    const NEWS_COVER_IMAGE_WIDTH = 370;
    const NEWS_COVER_IMAGE_HEIGHT = 250;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.article-news.index')->with(['type' => Constant::NEWS_TYPE,'title' => 'لیست خبرها']);
    }

    public function list(ListRequest $request)
    {
        $page = $request->input('page') ?? 1;
        $sort = $request->sort ?? 'id';
        $type = $request->type ?? 'desc';
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });;

        $items = News::orderBy($sort, $type)->with('user');
        $items = AdminCommonService::filter($items, $request);
        $counts = $items->count();
        $items = $items->paginate(Constant::ADMIN_LIST_COUNT);
        return view('admin.article-news.articles-table', compact('items', 'counts', 'page'))
            ->with(['type' => Constant::NEWS_TYPE]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return AdminCommonService::createArticle(Constant::NEWS_TYPE);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticleRequest $request)
    {
        return AdminCommonService::storeArticle($request, Constant::NEWS_TYPE);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit(string $slug)
    {
        $news = News::whereSlug($slug)->first();
        return AdminCommonService::editArticle($news, Constant::NEWS_TYPE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreArticleRequest $request
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function update(StoreArticleRequest $request, string $slug)
    {
        $news = News::whereSlug($slug)->first();
        return AdminCommonService::updateArticle($request, $news, Constant::NEWS_TYPE);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param News $news
     * @return \Illuminate\Http\Response
     */
    public function delete(News $news)
    {
        \Cache::forget(Constant::CACHE_NEWS_HOME_PAGE);
        return CommonService::delete($news);
    }

    public function disapproval(News $news)
    {
        return AdminCommonService::deactive($news);
    }

    public function accept(News $news)
    {
        return AdminCommonService::active($news);
    }

}
