<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class PermissionController extends Controller
{
    public function index() {
    	return view('admin.access.permission.index');
    }

	public function list(Request $request)
	{
		$page = $request->page;
		Paginator::currentPageResolver(function () use ($page) {
			return $page;
		});
		$permissions = Permission::latest();
		$permissions = $permissions->paginate(12);
        return view('admin.access.permission.permissions-table', compact('permissions'));
	}

		public function update()
		{

		}
}
