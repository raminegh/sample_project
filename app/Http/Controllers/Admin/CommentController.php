<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Comment;
use App\Constant;
use App\Events\HaveNotification;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Psy\Util\Str;

class CommentController extends Controller
{
    public function index()
    {
        return view('admin.comment.index');
    }


    public function list(Request $request)
    {
        $page = $request->page ?? 1;
        $type = $request->type;
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        $comments = Comment::orderBy('id', 'DESC')->where('parent_id', 0)->where('commentable_type', $type)->latest()->with('comments');
        $comments = $comments->paginate(Constant::ADMIN_LIST_COUNT);
        $comments = view('admin.comment.list', compact('comments', 'type'));
        return $comments;
    }

    public function toggleApproved(Request $request) {
        $approved = $request->input('approved');
        $comment_id = $request->input('comment_id');
        $comment = Comment::where('id', $comment_id)->first();
        $post = $comment->commentable;
        $comment->approved = $approved;

        if($approved == 1) {
            $post->increment('comment_count');
            if($comment->commentable_type == Constant::ARTICLE_TYPE || $comment->commentable_type == Constant::NEWS_TYPE) {
                event(new HaveNotification($comment->commentable->user->id, Notification::COMMENT, $comment->commentable_type, $comment->commentable_id));
            } else if($comment->commentable_type == Constant::IMAGE_TYPE) {
                event(new HaveNotification($comment->commentable->imageable->id, Notification::COMMENT, $comment->commentable_type, $comment->commentable_id));
            } else if($comment->commentable_type == Constant::VIDEO_TYPE) {
                event(new HaveNotification($comment->commentable->videoable->user->id, Notification::COMMENT, $comment->commentable_type, $comment->commentable_id));
            }
            if($comment->parent_id) {
                $repliedComment = Comment::where('id', $request->parent_id)->first();
                event(new HaveNotification($repliedComment->user->id, Notification::REPLY, $comment->commentable_type, $comment->commentable_id));
            }
            if(mb_strstr($comment->comment, '@')){
                $wordsArray = explode(' ', $comment->comment);
                $usersMentioned = [];
                foreach ($wordsArray as $key => $word){
                    if(!in_array($word, $usersMentioned)){
                        if($word['0'] == '@'){
                            $usersMentioned[] = $word;
                            $word = ltrim($word, '@');
                            $user = User::where('username', $word)->first();
                            if($user) {
                                $comment->comment = str_replace('@' . $word, "<a href='/$word'>" . "@" . $word . "</a>", $comment->comment);
                                event(new HaveNotification($user->id, Notification::MENTION, $comment->commentable_type, $comment->commentable_id));
                            }

                        }
                    }
                }
            }
        } else {
            $post->decrement('comment_count');
        }

        $comment->save();
        return ['status' => true];
    }

    public function addComment(Request $request) {
        auth()->user()->comments()->create([
            'user_id' => auth()->user()->id,
            'comment' => $request->comment,
            'parent_id' => $request->comment_id,
            'commentable_id' => $request->commentable_id,
            'commentable_type' => $request->commentable_type,
            'name' => auth()->user()->name,
            'approved' => 1
        ]);

        $repliedComment = Comment::findOrFail($request->parent_id)->first();
        if($request->comment_id) {
            event(new HaveNotification($repliedComment->user_id, Notification::REPLY, $request->commentable_type, $request->likeable_id));
        }

        return ['status' => true];
    }

    public function delete(Request $request) {
        $comment_id = $request->input('comment_id');
        $comment = Comment::find($comment_id);
        $comment->comments()->delete();
        $result = $comment->delete();

        if (!$result) {
            return ['status' => false];
        }
        return ['status' => true];

    }
}
