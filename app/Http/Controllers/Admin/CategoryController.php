<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\CategoryItem;
use App\Constant;
use App\Http\Requests\Admin\DeleteRequest;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Services\Admin\AdminCommonService;
use App\Http\Services\CommonService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class CategoryController extends Controller
{

    public function index()
    {
        return view('admin.category.index');
    }

    public function list(Request $request)
    {
        $categories = CommonService::getParentsCategories($request->type);
        return view('admin.section.categories-label', compact('categories'));
    }

    public function getCategories(Request $request)
    {
        $options = CommonService::getCategories($request->type);
        return view('admin.section.options', compact('options'));
    }

    public function store(StoreCategoryRequest $request)
    {
        $result = Category::create([
            'name' => $request->name,
            'label' => $request->label,
            'parent_id' => $request->parent_id,
            'type' => $request->type
        ]);
        if (!$result)
            return toJson(['status' => false, 'message' => "در ایجاد دسته بندی جدید مشکلی بوجود آمد"]);

        \Cache::forget(Constant::CACHE_CATEGORIES_ARTICLE);
        \Cache::forget(Constant::CACHE_CATEGORIES_MEDIA);
        return toJson(['status' => true, 'message' => "دسته بندی جدید با موفقیت ایجاد شد."]);
    }

    public function update(UpdateCategoryRequest $request)
    {
        $category = Category::findOrFail($request->id);
        $result = $category->update([
            "name" => $request->name,
            "label" => $request->label,
            "parent_id" => $request->parent_id,
        ]);
        if (!$result)
            return toJson(['status' => false, 'message' => "در ایجاد دسته بندی جدید مشکلی بوجود آمد"]);

        \Cache::forget(Constant::CACHE_CATEGORIES_ARTICLE);
        \Cache::forget(Constant::CACHE_CATEGORIES_NEWS);
        \Cache::forget(Constant::CACHE_CATEGORIES_MEDIA);
        return toJson(['status' => true, 'message' => "دسته بندی جدید با موفقیت ایجاد شد."]);
    }

    public function delete(Category $category)
    {
        $hasContent = CategoryItem::where('category_id', $category->id)->first();
        if ($hasContent)
            return toJson(['status' => false, 'message' => "این دسته بندی حاوی محتواست به همین دلیل اجازه حذف وجود ندارد"]);

        if ($category->childs()->count() > 0)
            return toJson(['status' => false, 'message' => "امکان حذف دسته بندی هایی که زیر مجموعه دارند، وجود ندارد"]);

        $category->delete();
        \Cache::forget(Constant::CACHE_CATEGORIES_ARTICLE);
        \Cache::forget(Constant::CACHE_CATEGORIES_NEWS);
        \Cache::forget(Constant::CACHE_CATEGORIES_MEDIA);
        return toJson(['status' => true, 'message' => "آیتم موردنظر  با موفقیت حذف شد."]);
    }
}
