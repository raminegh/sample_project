<?php

namespace App\Http\Controllers\Web;

use App\Article;
use App\Constant;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\TopSalon;
use App\News;
use App\Settings;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function index()
    {
        $slides = \Cache::remember(Constant::CACHE_MAIN_SLIDES, Constant::TWO_HOUR_SECONDS, function () {
            return unserialize(get_setting(Settings::SLIDE_MAIN_PAGE));
        });

        $topSalons = \Cache::remember(Constant::CACHE_TOP_SALONS, Constant::TWO_HOUR_SECONDS, function (){
            return TopSalon::orderBy('priority', 'desc')->with('user', 'salon')->get();
        });

        $articles = \Cache::remember(Constant::CACHE_ARTICLES_HOME_PAGE, Constant::TWO_HOUR_SECONDS, function (){
            return Article::orderBy('id', 'desc')->take(8)->get();
        });

        $news = \Cache::remember(Constant::CACHE_NEWS_HOME_PAGE, Constant::THIRTY_MINUTE_SECONDS, function (){
            return News::orderBy('id', 'desc')->take(8)->get();
        });

        $services = \Cache::remember(Constant::CACHE_SERVICES_HOME_PAGE, Constant::TWO_HOUR_SECONDS, function (){
            $services = Service::all();
            if($services->count() > 0){
                return $services->random(floor(($services->count() / 2)) + 1);
            }
            return [];
        });

        return view('app.home.index', compact('slides', 'topSalons', 'articles', 'news', 'services'));
    }
}
