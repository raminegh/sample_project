<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Settings;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index()
    {
        $contactUs = get_setting(Settings::CONTACT_US);
        return view('app.contact-us.contact-us', compact('contactUs'));
    }
}
