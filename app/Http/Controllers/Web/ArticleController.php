<?php

namespace App\Http\Controllers\Web;

use App\Article;
use App\Constant;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommonListRequest;
use App\Http\Services\App\AppCommonService;
use App\Http\Services\CommonService;

class ArticleController extends Controller
{
    public function getArticles()
    {
        $categories = CommonService::getParentsCategories(Constant::NEWS_TYPE);
        return view('app.article.index', compact('categories'));
    }

    public function list(CommonListRequest $request)
    {
        return AppCommonService::ajaxList($request, Constant::ARTICLE_TYPE);
    }

    public function show($slug)
    {
        $article = Article::where('slug', $slug)->with('comments', 'user', 'tags')->firstOrFail();
        return AppCommonService::showArticle($article);
    }
}
