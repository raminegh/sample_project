<?php

namespace App\Http\Controllers\Web;

use App\Article;
use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Employment;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class SitemapController extends Controller
{
    public function index()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('app.sitemap', 1440);
        if(! $sitemap->isCached()) {
            $sitemap->add(URL::to('/'), '2020-01-25T20:10:00+02:00', '1.0', 'daily');
            $sitemap->add(URL::to('/article'), '2020-01-25T20:10:00+02:00', '1.0', 'daily');
            $sitemap->add(URL::to('/news'), '2020-01-25T20:10:00+02:00', '0.9', 'daily');
            $sitemap->add(URL::to('/employment'), '2020-01-25T20:10:00+02:00', '0.9', 'daily');
            $sitemap->add(URL::to('/discount'), '2020-01-25T20:10:00+02:00', '0.9', 'daily');
        }
        return $sitemap->render();
    }

    public function articles()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('app.sitemap.articles', 1440);
        if(! $sitemap->isCached()) {
            $articles = Article::latest()->get();
            foreach ($articles as $article) {
                $sitemap->add(URL::to($article->path()), $article->created_at, '0.9', 'weekly');
            }
        }
        return $sitemap->render();
    }

    public function news()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('app.sitemap.news', 1440);
        if(! $sitemap->isCached()) {
            $news = News::latest()->get();
            foreach ($news as $new) {
                $sitemap->add(URL::to($new->path()), $new->created_at, '0.9', 'weekly');
            }
        }
        return $sitemap->render();
    }

    public function employments()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('app.sitemap.employments', 1440);
        if(! $sitemap->isCached()) {
            $employments = Employment::latest()->get();
            foreach ($employments as $employment) {
                $sitemap->add(URL::to(route('app.employment.show', $employment)), $employment->created_at, '0.9', 'weekly');
            }
        }
        return $sitemap->render();
    }

    public function discounts()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('app.sitemap.discounts', 1440);
        if(! $sitemap->isCached()) {
            $discounts = Discount::latest()->get();
            foreach ($discounts as $discount) {
                $sitemap->add(URL::to(route('app.discount.show', $discount)), $discount->created_at, '0.9', 'weekly');
            }
        }
        return $sitemap->render();
    }
}
