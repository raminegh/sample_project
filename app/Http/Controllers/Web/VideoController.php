<?php
namespace App\Http\Controllers\Web;

use App\Category;
use App\CategoryItem;
use App\Constant;
use App\Events\UploadNewVideo;
use App\Events\VideoTestEvent;
use App\Events\VisitVideoEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\App\Video\SetInfoVideoRequest;
use App\Http\Requests\App\Video\UploadVideoRequest;
use App\Http\Services\AppCommonService;
use App\Http\Services\CommonService;
use App\Like;
use App\models\Ads;
use App\Models\Album;
use App\Models\Video;
use App\Tag;
use App\Taggable;
use Carbon\Carbon;
use FFMpeg\Filters\Video\CustomFilter;
use FFMpeg\Format\Video\X264;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Pbmedia\LaravelFFMpeg\FFMpegFacade;
use Pbmedia\LaravelFFMpeg\Media;

class VideoController extends Controller
{
    const VIDEO_COVER_IMAGE_WIDTH = 720;
    const VIDEO_COVER_IMAGE_HEIGHT = 480;
    const VIDEO_TEMP_PATH = 'TMP';
    const VIDEO_ORIGIANL_PATH = 'ORIGINAL';

    public function index()
    {
        $categories = CommonService::getParentsCategories(Category::TYEP_MEDIA);
        return view('app.video.index', compact('categories'));
    }

    public function list(Request $request)
    {
        return AppCommonService::ajaxList($request, Constant::VIDEO_TYPE);
    }

    public function create()
    {
        $categories = CommonService::getCategories(Category::TYEP_MEDIA);
        $alubms = auth()->user()->albums()->whereType('video')->get();
        return view('app.video.upload', compact('categories', 'alubms'));
    }

    public function upload(UploadVideoRequest $request)
    {
        $videoTmpPath = $this->getVideoPath(self::VIDEO_TEMP_PATH);
        try {
            $video = $request->file('video');
            $fileName = '';
            $videoModel = true;
            while ($videoModel) {
                $fileName = Str::random(5);
                $videoModel = Video::where('video_id', $fileName)->first();
            }

            Storage::disk('videos')->put($videoTmpPath . $fileName, $video->get());
            Video::create([
                'video_id' => $fileName,
                'user_id' => auth()->id(),
            ]);

            return response(['video_id' => $fileName], 200);
        } catch (Exception $e) {
            return response(['message' => 'خطایی رخ داده است'], 500);
        }
    }

    public function save(SetInfoVideoRequest $request)
    {
        $videoable_id = $request->videoable_id; // id of album
        $album = Album::find($videoable_id);
        $this->checkAlbum($album);
        $video = Video::where('video_id', $request->video_id)->first();
        $coverUrl = $this->getCoverUrl($request->file('cover'), $video);
        $subtitlesUrl = $this->getSubtitlesUrl($request->file('subtitle_fa'), $request->file('subtitle_en'));

        $video->update([
            'title' => $request->title,
            'description' => $request->description,
            'cover' => $coverUrl,
            'videoable_id' => $album->id,
            'videoable_type' => 'album',
            'subtitles' => $subtitlesUrl,
            'hls_state' => Video::PENDING_STATE,
            'download_state' => Video::PENDING_STATE
        ]);

        $watermark = $this->getWaterMark($request->watermark);
        $tmpPath = $this->getVideoPath(self::VIDEO_TEMP_PATH);
        $path = $this->getVideoPath(self::VIDEO_ORIGIANL_PATH);
        event(new UploadNewVideo($video, $tmpPath, $path, $request->video_id, $watermark));
        setTags($request->tags, $video);
        setCategories($request->category_id, $video);
        return redirect()->route('app.profile.index')->with(['success' => "ویدیو شما در حال پردازش است و پس از اتمام پردازش و تایید، نمایش داده خواهد شد"]);
    }

    public function show(Video $video)
    {
        if($video->hls_state == Video::ACCEPTED_STATE || isAdmin()){
            $category_id = $video->categories[0];
            $user = $video->user;
            $comments = $video->approvedComments;
            $isLike = in_array(auth()->id(), $video->likes()->pluck('user_id')->toArray());
            $isFollow = in_array(auth()->id(), $user->followers()->pluck('id')->toArray());
            $tags = $video->tags;
            $ad = Ads::Ads('teaser')->first();
            $overonVideos = $user->approvedVideos()->take(4)->get();
            event(new VisitVideoEvent($video));
            return view('app.video.show', compact('video', 'comments', 'user', 'tags', 'category_id', 'ad', 'overonVideos'))->with([
                'isLike' => $isLike,
                'isFollow' => $isFollow
                ]);
        }

        abort(404);
    }

    public function delete(Video $video)
    {
        return CommonService::delete($video);
    }


    /* ###### helper functions ###### */

    private function getSubtitlesUrl($subtitleFa, $subtitleEn) {
        if($subtitleFa || $subtitleEn) {
            $subtitlesUrl = null;
            if ($subtitleFa) {
                $subtitlesUrl[] = ['label' => "فارسی", 'src_lang' => "fa", 'url' => uploadFile($subtitleFa, 'subtitles')];
            }

            if ($subtitleEn) {
                $subtitlesUrl[] = ['label' => "English", 'src_lang' => "en", 'url' => uploadFile($subtitleEn, 'subtitles')];
            }
            return $subtitlesUrl;
        }

        return null;
    }

    private function getWaterMark($watermark) {
        if ($watermark) {
            return request()->getHttpHost() . '/' . auth()->user()->username;
        }
        return null;
    }

    private function checkAlbum(Album $album) {
        if($album->type == 'image') {
            return redirect()->back()->with(['error' => "امکان آپلود تصویر در آلبوم ویدیو نیست."]);
        }

        if($album->user_id !== auth()->user()->id) {
            return redirect()->back()->with(['error' => "خطا، این آلبوم متعلق به شما نیست."]);
        }
    }

    private function getCoverUrl($cover, Video $video) {
        if($cover){
            $coverUrl = $video->cover;
            if($coverUrl) {
                deleteFile($coverUrl['original']);
                deleteFile($coverUrl['thumb']);
            }
            return coverProduction($cover, Constant::VIDEO_COVER_FOLDER, self::VIDEO_COVER_IMAGE_WIDTH, self::VIDEO_COVER_IMAGE_HEIGHT);
        }
        return null;
    }

    private function getVideoPath(string $type) {
        $path = "/upload/videos" . getDatePath();
        if ($type === self::VIDEO_TEMP_PATH) {
            return $path . auth()->id() . '/tmp/';
        } else if ($type === self::VIDEO_ORIGIANL_PATH) {
            return $path . auth()->id() . '/';
        }
        return '';
    }

}
