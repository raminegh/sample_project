<?php

namespace App\Http\Controllers\Web;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Http\Requests\App\User\SetSalonInfoRequest;
use App\Http\Requests\App\User\StoreUserInfoRequest;
use App\Http\Services\AppCommonService;
use App\Models\Personnel;
use App\Models\PositionRequest;
use App\Models\Province;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return view('app.user.index');
    }

    public function list(Request $request)
    {
        return AppCommonService::ajaxList($request, Constant::USER_TYPE);
    }

    public function userInfo()
    {
        return view('app.user.set-info');
    }

    public function setUserInfo(StoreUserInfoRequest $request)
    {
        $username = $request->username;
        $password = $request->password;
        $name = $request->name;
        $bio = $request->bio;
        $website = $request->website;
        $instagram = $request->instagram;
        $avatar = $request->file('avatar');
        $level = $request->level;

        $user = auth()->user();
        $user->username = $username;
        $user->name = $name;
        $user->level = $level;
        $user->password = $password;

        if($avatar) {
            $avatar = avatarImageUpload($avatar, 'avatar');
            $user->avatar = $avatar;
        }

        $profile = $user->profile;
        $profile->bio = $bio;
        $profile->website = $website;
        $profile->instagram = $instagram;
        $profile->save();
        \Cache::forget(Constant::CACHE_NAME_USER . auth()->id());
        \Cache::forget(Constant::CACHE_AVATAR_USER . auth()->id());
        switch ($user->level) {
            case User::USER:
                $user->is_active = 1;
                $user->save();
                return redirect()->route('app.profile.index')->with(['success' => 'اطلاعات شما با موفقیت ثبت شد.']);
                break;
            case User::BEAUTY_ACTIVIST:
                $user->is_active = 1;
                $user->save();
                return redirect()->route('app.user.employInfo');
                break;
            case User::BUSINESS_OWNER:
                $user->is_active = 1;
                $user->save();
                return redirect()->route('app.user.salonInfo');
                break;
        }
        $user->save();

        \Cache::forget(Constant::CACHE_NAME_USER . auth()->id());
        \Cache::forget(Constant::CACHE_AVATAR_USER . auth()->id());

    }

    public function salonInfo()
    {
        $provinces = Province::all();
        return view('app.user.set-salon', compact('provinces'));
    }

    public function employInfo()
    {
        return view('app.user.set-employ-info');
    }

    public function setEmployInfo(Request $request)
    {
        $this->validate($request, [
            'is_employ' => "required|in:0,1",
            'place_work' => "nullable|string",
            'positions_id' => 'required|array',
            'positions_id.*' => 'integer|min:1|exists:positions,id'
        ]);
        $user = auth()->user();
        $user->is_employed = $request->input('is_employ');
        $user->is_active = 1;
        $user->save();
        $profile = $user->profile;
        $profile->place_work = $request->place_work;

        auth()->user()->positions()->sync($request->input('positions_id'));

        return redirect()->route('app.profile.index')->with(['success' => 'اطلاعات شما با موفقیت ثبت شد.']);
    }

    public function setEmployRequest(Request $request)
    {
        $this->validate($request, [
            'description' => "nullable|string|min:5",
        ]);
        $user = auth()->user();
        $user->is_employed = 0;
        $user->is_active = 1;
        $user->save();


        PositionRequest::create([
            'user_id' => auth()->id(),
            'description' => $request->description
        ]);

        return redirect()->route('app.profile.index')->with(['success' => 'اطلاعات شما با موفقیت ثبت شد.']);
    }

    public function setSalonInfo(SetSalonInfoRequest $request)
    {
        $salon = auth()->user()->salon()->create([
            'business_id' => $request->business_id,
            'name' => $request->name,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'description' => $request->description,
            'manager' => $request->manager,
            'tel' => $request->tel,
            'address' => $request->address,
            'lat' => $request->lat,
            'lng' => $request->lng
        ]);

        Personnel::create([
            'salon_id' => $salon->id,
            'user_id' => auth()->id()
        ]);

        $salon->services()->attach($request->services_id);
        $user = auth()->user();
        $user->is_active = 1;
        $user->save();
        \Cache::forget(Constant::CACHE_NAME_USER . auth()->id());
        \Cache::forget(Constant::CACHE_AVATAR_USER . auth()->id());
        return redirect()->route('app.profile.index')->with(['success' => 'اطلاعات شما با موفقیت ثبت شد.']);

    }

    public function resetPassword()
    {
        return view('app.dashboard.reset-password.reset-password');
    }

    public function setPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|confirmed|min:6|max:32'
        ]);

        $user = auth()->user();
        $user->update([
            "password" => $request->password
        ]);

        return redirect()->back()->with(['success' => "گذرواژه با موفقیت تغییر پیدا کرد"]);
    }

}
