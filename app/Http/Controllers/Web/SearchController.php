<?php

namespace App\Http\Controllers\Web;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Employment;
use App\Models\Image;
use App\Models\Salon;
use App\Models\Video;
use App\News;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $this->validate($request, [
            'q' => 'required|string|min:2|max:64',
            'type' => 'required|string|in:article,new,video,image,employment,discount,salon,user',
            'page' => 'nullable|integer|min:1'
        ]);
        $page = $request->page ?? 1;
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $keywords = $request->q;
        switch ($request->type){
            case 'article':
                $articles = Article::search($keywords)->latest()->paginate(20);
                $categories = Category::all();
                return view('app.article.index', compact('articles', 'categories'));
                break;
            case 'new':
                $news = News::search($keywords)->latest()->paginate(20);
                return view('app.news.index', compact('news'));
                break;
            case 'employment':
                $employments = Employment::search($keywords)->latest()->paginate(20);
                return view('app.employment.public.search', compact('employments'));
                break;
            case 'discount':
                $discounts = Discount::search($keywords)->latest()->paginate(20);
                return view('app.discount.public.search', compact('discounts'));
                break;
            case 'video':
                $videos = Video::search($keywords)->latest()->paginate(20);
                return view('app.video.public.search', compact('videos'));
                break;
            case 'image':
                $images = Image::search($keywords)->latest()->paginate(20);
                return view('app.image.public.search', compact('images'));
                break;
            case 'salon':
                $salons = Salon::search($keywords)->latest()->paginate(20);
                return view('app.salon.search', compact('salons'));
                break;
            case 'user':
                $users = User::search($keywords)->latest()->paginate(20);
                return view('app.user.public.search', compact('users'));
                break;
        }

    }
}
