<?php

namespace App\Http\Controllers\Web;

use App\Article;
use App\Constant;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Collection\TagCollection;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class TagController extends Controller
{
    public function getRelated(string $type)
    {
        $name = request()->name;
        $tag = Tag::whereName($name)->firstOrFail();
        $page = request()->page;

        switch ($type) {
            case Constant::ARTICLE_TYPE;
                Paginator::currentPageResolver(function () use ($page) {
                    return $page;
                });
                $articles = Article::orderBy('id', 'DESC')->with('user');
                $articles = $articles->paginate(12);
                $articles = $tag->articles;
                return view('app.article.search', compact('articles'));
            break;
            case Constant::NEWS_TYPE;
                $news = $tag->news;
                return view('app.news.index', compact('news'));
            break;
            case Constant::EMPLOYMENT_TYPE;

            break;
            case Constant::DISCOUNT_TYPE;

            break;
            case Constant::VIDEO_TYPE;

            break;
            case Constant::IMAGE_TYPE;

            break;
        }
        return view('app.tag.index');
    }

    public function ajaxSearch(Request $request)
    {
        $this->validate($request, [
           'search' => 'nullable|string'
        ]);
        $search = $request->search;
        if ($search == '') {
            $tags = Tag::orderby('id', 'asc')
                ->select('id', 'name')
                ->limit(5)
                ->get();
        } else {
            $tags = Tag::orderby('id', 'asc')
                ->select('id', 'name')
                ->where('name', 'like', '%' . $search . '%')
                ->limit(5)
                ->get();
        }

        $data = [];
        foreach ($tags as $tag){
            $data[] = ["text" => $tag['name'], "id" => $tag['id']];
        }

        return json_encode($data);
    }
}
