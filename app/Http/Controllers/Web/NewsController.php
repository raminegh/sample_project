<?php

namespace App\Http\Controllers\Web;

use App\Category;
use App\Constant;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommonListRequest;
use App\Http\Services\App\AppCommonService;
use App\Http\Services\CommonService;
use App\News;

class NewsController extends Controller
{
    public function index()
    {
        $categories = CommonService::getParentsCategories(Category::TYEP_NEWS);
        return view('app.news.index', compact('categories'));
    }

    public function list(CommonListRequest $request)
    {
        return AppCommonService::ajaxList($request, Constant::NEWS_TYPE);
    }


    public function show($slug)
    {
        $news = News::where('slug', $slug)->with('comments', 'user', 'tags')->firstOrFail();
        return AppCommonService::showArticle($news, Constant::NEWS_TYPE);
    }

}
