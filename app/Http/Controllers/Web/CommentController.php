<?php

namespace App\Http\Controllers\Web;

use App\Article;
use App\Comment;
use App\Constant;
use App\Events\HaveNotification;
use App\Http\Controllers\Controller;
use App\Http\Requests\App\Comment\StoreCommentRequest;
use App\Models\Image;
use App\Models\Notification;
use App\Models\Video;
use App\News;
use App\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(StoreCommentRequest $request)
    {

        if(auth()->check()) {
            if(auth()->user()->comments()->where('created_at', '>', now()->subHours())->count() >= 90){
                return response()->json([
                    'status' => false,
                    'شما بیشتر از حد مجاز در ساعت ، نظر داده اید، لطفا بعدا امتحان کنید.'
                ]);
            }
        }

        $commentOwner = null;
        switch ($request->commentable_type) {
            case Constant::ARTICLE_TYPE:
                $commentOwner = Article::find($request->commentable_id);
                break;
            case Constant::NEWS_TYPE:
                $commentOwner = News::find($request->commentable_id);
                break;
            case Constant::VIDEO_TYPE:
                $commentOwner = Video::find($request->commentable_id);
                break;
            case Constant::IMAGE_TYPE:
                $commentOwner = Image::find($request->commentable_id);
                break;
            default:
                $commentOwner = null;
        }

        if (!$commentOwner) {
            return response()->json([
                'status' => false,
                'message' => "خطا"
            ]);
        }

        $name = '';
        $userId = 0;
        if (auth()->check()) {
            if (auth()->user()->level == User::BUSINESS_OWNER) {
                $name = auth()->user()->salon->name;
            } else {
                $name = auth()->user()->name;
            }
            $userId = auth()->id();
        } else {
            $request->name ? $name = $request->name : $name = 'کاربر میهمان';
        }

        $comment = new Comment();
        $comment->commentable()->associate($commentOwner);
        $comment->name = $name;
        $comment->user_id = $userId;
        $comment->parent_id = $request->parent_id ? $request->parent_id : 0;
        $comment->comment = $request->comment;
        $comment->email = $request->email ? $request->email : null;
        $comment->save();

        return response()->json([
            'status' => true
        ]);
    }
}
