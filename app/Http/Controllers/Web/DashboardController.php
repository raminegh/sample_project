<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Services\VideoService;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $profile = $user->profile;
        $unreadMessages = $user->messages()->where('is_read', 0)->get()->count();
        if($user->level != \App\User::BUSINESS_OWNER) {
            return view('app.dashboard.index', compact(
                'user',
                'profile',
                'unreadMessages'
            ));
        }
        $salon = $user->salon;
        $visitsToday = $salon->visits()->whereDate('created_at', now())->get()->count();
        $visitsYesterday = $salon->visits()->whereDate('created_at', now()->subDays(1))->get()->count();
        return view('app.dashboard.index', compact('user',
            'salon',
            'profile',
            'visitsToday',
            'visitsYesterday',
        'unreadMessages'
        ));
    }

    public function videoIndex()
    {
        return view('app.dashboard.video.index');
    }

    public function Videolist(Request $request, string $username)
    {
        return VideoService::ajaxList($request, $username);
    }

    public function imageIndex()
    {
        return view('app.dashboard.image.index');
    }

    public function ImageList(Request $request, string $username)
    {
        return VideoService::ajaxList($request, $username);
    }

}
