<?php

namespace App\Jobs;
use App\Models\Video;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Filters\Video\CustomFilter;
use FFMpeg\Format\Video\X264;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Pbmedia\LaravelFFMpeg\FFMpegFacade;
use Pbmedia\LaravelFFMpeg\Media;

class ConvertAndWaterMarkToUploadedVideoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Video
     */
    private $video;
    /**
     * @var string
     */
    private $tmpPath;
    /**
     * @var string
     */
    private $finalPath;
    /**
     * @var string
     */
    private $filename;
    /**
     * @var string|null
     */
    private $watermark;

    const P2160 = ['w' => 3840, 'h' => 2160];
    const P1440 = ['w' => 2560, 'h' => 1440];
    const P1080 = ['w' => 1920, 'h' => 1080];
    const P720 = ['w' => 1280, 'h' => 720];
    const P480 = ['w' => 854, 'h' => 480];
    const P360 = ['w' => 640, 'h' => 360];
    const P240 = ['w' => 426, 'h' => 240];

    const DIMENTIONS_ARRAY = [
        self::P2160,
        self::P1440,
        self::P1080,
        self::P720,
        self::P480,
        self::P360,
        self::P240
    ];


    /**
     * Create a new job instance.
     *
     * @param Video $video
     * @param string $tmpPath
     * @param string $finalPath
     * @param string $filename
     * @param string $watermark
     */
    public function __construct(Video $video, string $tmpPath, string $finalPath, string $filename, string $watermark = null)
    {
        $this->video = $video;
        $this->tmpPath = $tmpPath;
        $this->finalPath = $finalPath;
        $this->filename = $filename;
        $this->watermark = $watermark;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        /** @var Media $videoUploadedInTmp */
        $videoUploadedInTmp = FFMpegFacade::fromDisk('videos')->open($this->tmpPath . $this->filename);
        $filter = null;
        Log::info("Watermark" . $this->watermark);
        if ($this->watermark) {
            $fontPath = '/usr/share/fonts/dejavu/DejaVuSansCondensed-Oblique.ttf';
            $filter = new CustomFilter("drawtext=fontfile='" . $fontPath . "':text='" . $this->watermark . "': fontcolor=white@0.7: fontsize=30:x=50: y=(h - text_h - 20)");
        }


        $path = $this->finalPath . '/hls/' . $this->filename;
        if(!$this->video->cover) {
            $coverPath = "/upload/images/" . 'video_cover' . getDatePath() . Str::random('6') . '.png';
            $videoUploadedInTmp
                ->getFrameFromSeconds(floor($videoUploadedInTmp->getDurationInSeconds() / 2))
                ->export()
                ->toDisk('videos')
                ->save($coverPath);

            $this->video->cover = ['thumb' => $coverPath, 'original' => $coverPath];
        }

        $dimenstionObj = getDimentionsFFMPEG($videoUploadedInTmp);

        $dimenstionsTmpArray = self::DIMENTIONS_ARRAY;
        $videoOriginalDimention = ['w' => $dimenstionObj->getWidth(), 'h' => $dimenstionObj->getHeight()];
        $newArray = [];
        $url = [];
        foreach ($dimenstionsTmpArray as $key => $dimension) {
            if ($dimension['w'] === $videoOriginalDimention['w'] && $dimension['h'] === $videoOriginalDimention['h']) {
                $newArray = array_slice($dimenstionsTmpArray, $key);
            }
        }


        foreach ($newArray as $key => $dimension) {
            $format = (new X264('libmp3lame', 'libx264'))->setKiloBitrate($dimension['h']);
            $videoUploadedInTmp->addFilter($filter)
                ->exportForHLS()
                ->toDisk('videos')
                ->addFormat($format, function ($media) use ($dimension){
                    $media->addFilter(function ($filters) use ($dimension) {
                        $filters->resize(new Dimension($dimension['w'], $dimension['h']));                    });
                })
                ->save($path . $dimension['h'] . '/' . $this->filename . $dimension['h'] . '.m3u8');
            $url[$dimension['h']] = $path . $dimension['h'] . '/' . $this->filename . $dimension['h'] . '.m3u8';
        }

        $this->video->hls_url = $url;
        $this->video->hls_state = Video::CONVERTED_STATE;
        $this->video->duration = $videoUploadedInTmp->getDurationInSeconds();
        $this->video->save();

        FFMpegFacade::cleanupTemporaryFiles();
    }







}
