<?php

use App\Category;
use App\Constant;
use App\Settings;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

/* ---------- Date ---------- */
function toJalali($date)
{
    if ($date) {
        if (!is_numeric($date)) {
            $date = strtotime($date);
            $date = date('Y-m-d', $date);
        }

        $date = explode('-', $date);
        $date = \Morilog\Jalali\CalendarUtils::toJalali($date[0], $date[1], $date[2]);
        $date[1] = twoDigitNumber($date[1]);
        $date[2] = twoDigitNumber($date[2]);
        return implode('/', $date);
    }
    return '---';
}

function toGregorian($date)
{
    if ($date) {
        if ($date === '0000/00/00') {
            return null;
        }
        $date = explode('/', $date);
        $date = array_map(function ($item) {
            if (!is_numeric($item))
                return 1;
            return $item;
        }, $date);
        $date = \Morilog\Jalali\CalendarUtils::toGregorian($date[0], $date[1], $date[2]);
        $date[1] = twoDigitNumber($date[1]);
        $date[2] = twoDigitNumber($date[2]);
        return implode('-', $date);
    }
    return '---';
}

function convertJalaliToGregorian($date)
{
    return toGregorian(convert2english($date));
}

function getDateAgo($date)
{
    return \Morilog\Jalali\Jalalian::forge($date)->ago();
}

/* ---------- Text ---------- */

function shortFarsiString($string, $maxSize = 17)
{
    if (mb_strlen($string) > $maxSize) {
        return mb_substr($string, 0, $maxSize, 'utf-8') . '...';
    }
    return $string;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


function shorterTitle($title)
{
    return mb_strlen($title) > 56 ? mb_substr($title, 0, 56) . '...' : $title;
}

function convert_br_to_newline($text)
{
    $breaks = array("<br />", "<br>", "<br/>");
    $text = str_ireplace($breaks, "\r\n", $text);
    return $text;
}

/* ---------- Number ---------- */

function convert2english($string)
{
    $persinaDigits1 = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    $persinaDigits2 = array('٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١', '٠');
    $allPersianDigits = array_merge($persinaDigits1, $persinaDigits2);
    $replaces = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    return str_replace($allPersianDigits, $replaces, $string);
}

function twoDigitNumber($number)
{
    return $number < 10 ? '0' . $number : $number;
}

function generateRandomNumber($lenght = 5)
{
    return $lenght < 1 ? 0 : rand(10 ** ($lenght - 1), (10 ** $lenght) - 1);
}

/* ---------- IMAGE ---------- */
function coverProduction($cover, $type, $width, $height)
{
    $name = generateRandomString() . generateRandomNumber(6) . '.' . $cover->getClientOriginalExtension();
    $url = [];
    $path = "/upload/images/" . $type . getDatePath();
    if_is_not_dir_mkdir($path);
    $cover = $cover->move(public_path() . $path, $name);
    $url['original'] = $path . $name;
    $thumbPath = $path . 'thumb/';
    if_is_not_dir_mkdir($thumbPath);
    Image::make($cover->getRealPath())->resize($width, $height)->save(public_path($thumbPath) . $name);
    $url['thumb'] = $thumbPath . $name;
    return $url;
}

function saveImage($file, $type = null, $width = null, $height = null, $watermark = null, $name = null)
{
    if (!$file) {
        return null;
    }
    $extention = $file->getClientOriginalExtension();
    if (!$name) {
        $randomNumber = generateRandomNumber(6);
        $name = generateRandomString() . $randomNumber . '.' . $extention;
    } else {
        $name = $name . '.' . $extention;
    }
    $path = "/upload/images/" . $type . getDatePath();
    $thumbPath = $path . 'thumb/';
    if_is_not_dir_mkdir($path);
    if_is_not_dir_mkdir($thumbPath);
    $url = $path . $name;
    if ($height && $width) {
        $url = [];
        Image::make($file->getRealPath())->resize($width, $height)->save(public_path($thumbPath) . $name);
        $url['thumb'] = $thumbPath . $name;
        $url['original'] = $path . $name;
    }
    $image = Image::make($file->getRealPath());
    if ($watermark) {
        $fontSize = 54;
        $margin = 70;
        if ($image->height() <= 480) {
            $fontSize = 18;
            $margin = 10;
        } else if ($image->height() <= 720) {
            $fontSize = 24;
            $margin = 16;
        } else if ($image->height() <= 1080) {
            $fontSize = 28;
            $margin = 20;
        } else if ($image->height() <= 1440) {
            $fontSize = 32;
            $margin = 24;
        } else if ($image->height() <= 2160) {
            $fontSize = 42;
            $margin = 32;
        }
        $height = $image->height() - $margin;
        $image->text($watermark, $margin, $height, function ($font) use($fontSize) {
            $font->file(public_path('fonts/imageFont/Xerox Sans Serif Narrow Bold Oblique.ttf'));
            $font->size($fontSize);
            $font->color(array(225, 225, 225, 0.8));
            $font->align('left');
            $font->valign('bottom');
            $font->angle(0);
        });
    }
    $image->save(public_path() . $path . $name);
    return $url;
}

function saveAdImage($file)
{
    if ($file) {
        $extention = $file->getClientOriginalExtension();
        $randomNumber = generateRandomNumber(6);
        $name = generateRandomString() . $randomNumber . '.' . $extention;
        $path = "/upload/images/ads" . getDatePath();
        if_is_not_dir_mkdir($path);
        Image::make($file->getRealPath())->resize(320, 240)->save(public_path($path) . $name);
        return $path . $name;
    }
    return null;
}

function slideProduction($cover, $type, $width, $height)
{
    $extention = $cover->getClientOriginalExtension();
    $name = str_replace('.' . $extention, '', $cover->getClientOriginalName());
    $randomNumber = rand(1000, 9999);
    $name = $name . $randomNumber . '.' . $extention;
    $path = "/upload/images/" . $type . getDatePath();
    if_is_not_dir_mkdir($path);
    $cover = $cover->move(public_path() . $path, $name);
    Image::make($cover->getRealPath())->resize($width, $height)->save(public_path($path) . $name);
    return $path . $name;
}

function avatarImageUpload($file, $folder)
{
    $imagePath = "/upload/images" . getDatePath() . "$folder/";
    if_is_not_dir_mkdir($imagePath);
    $randomStr = generateRandomString(2);
    $filename = $randomStr . $file->getClientOriginalName();
    $img = Image::make($file);
    $img->fit(400)->save(public_path($imagePath) . $filename, 100);
    return $imagePath . $filename;
}

function resize($path, $sizes, $imagePath, $filename)
{
    $images['original'] = $imagePath . $filename;
    foreach ($sizes as $size) {
        $images[$size] = $imagePath . "{$size}_" . $filename;
        Image::make($path)->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path($images[$size]));
    }
    return $images;
}


/* ---------- SETTINGS ---------- */

function update_setting($key, $value)
{
    $setting = Settings::firstOrNew([
        'key' => $key
    ]);
    $setting->value = $value;
    return $setting->save();
}

function get_setting($key)
{
    return optional(Settings::where('key', $key)->first())->value;
}

function getDatePath()
{
    $year = Carbon::now()->year;
    $month = Carbon::now()->month;
    $day = Carbon::now()->day;
    return "/{$year}/{$month}/{$day}/";
}

function uploadVideo($file, $folder)
{
    $videoPath = "/upload/videos" . getDatePath() . "$folder/";
    $strRandom = Str::random(16);
    $filename = $strRandom . '.mp4';
    $file->move(public_path($videoPath), $filename);
    return $videoPath . $filename;
}

function uploadFile($file, $folder)
{
    if ($file) {
        $filePath = "/upload/{$folder}" . getDatePath();
        $strRandom = Str::random(16);
        $filename = $strRandom . '.' . $file->getClientOriginalExtension();
        $file->move(public_path($filePath), $filename);
        return $filePath . $filename;
    }
    return null;
}

function if_is_not_dir_mkdir($dir)
{
    if (!is_dir(public_path($dir))) {
        File::makeDirectory(public_path() . $dir, 0777, true);
    }
}

function deleteFile($path)
{
    if ($path) {
        $path = public_path() . $path;
        if (file_exists($path)) {
            unlink($path);
        }
    }

}

function isAdmin()
{
    return auth()->user()->level == \App\User::ADMIN ? true : false;
}

function getName(\App\User $user)
{
    return $name = \Cache::remember(Constant::CACHE_NAME_USER . $user->id, Constant::ONE_DAY_SECONDS, function () use ($user) {
        return $user->level == \App\User::BUSINESS_OWNER ? $user->salon->name : $user->name;
    });
}

function tagsToString($tags) {
    $tagsStr = '';
    $len = count($tags);
    foreach ($tags as $key => $tag) {
        $key === ($len - 1) ? $tagsStr .= $tag->name : $tagsStr .= $tag->name . ',';
    }
    return $tagsStr;
}

function getAvatar(\App\User $user)
{
    return $name = \Cache::remember(Constant::CACHE_AVATAR_USER . $user->id, Constant::ONE_DAY_SECONDS, function () use ($user) {
        if (!$user->avatar) {
            return $user->level == \App\User::BUSINESS_OWNER ? Constant::SALON_DEFAULT_AVATAR : Constant::USER_DEFAULT_AVATAR;
        }
        return $user->avatar;
    });
}

function setTags($tags, $model)
{
    if ($tags) {
        $tagsArray = [];
        foreach ($tags as $tag) {
            if (!is_numeric($tag)) {
                $tagObj = Tag::create([
                    'name' => $tag,
                    'user_id' => auth()->id()
                ]);
                $tagsArray[] = $tagObj->id;
            } else {
                $tagsArray[] = $tag;
            }

        }
        $model->tags()->sync($tagsArray);
    }
}

function setCategories($categories, $model) {
    if ($categories) {
        if(!is_array($categories))
            $categories = [$categories];
        $finalCategories = [];
        foreach ($categories as $categoryId) {
            $category = Category::find($categoryId);
            $finalCategories[] = $categoryId;
            while ($category->parent_id) {
                $finalCategories[] = $category->parent_id;
                $category = Category::find($category->parent_id);
            }
        }
        $model->categories()->sync($finalCategories);
    }
}

function getDimentionsFFMPEG($video)
{
    return $video->getStreams()
        ->videos()
        ->first()
        ->getDimensions();
}

function getRowNumber($counts = 0, $page = 1, $key = 0)
{
    return $counts - (Constant::ADMIN_LIST_COUNT * ($page - 1) + $key);
}

function showStatus($model, $type = 'approved')
{
    switch ($type) {
        case 'is_active':
            $title = "فعال است";
            if ($model->is_active) {
                return '<i class="fas fa-check-circle text-success font-size-150p" data-toggle="tooltip" title="' . $title . '"></i>';
            }
            $title = "غیرفعال است";
            return '<i class="far fa-times-circle text-warning font-size-150p" data-toggle="tooltip" title="' . $title . '"></i>';
        case 'approved':
            $title = "تأیید شده";
            if ($model->is_approved) {
                return '<i class="fas fa-check-circle text-success font-size-150p" data-toggle="tooltip" title="' . $title . '"></i>';
            }
            $title = $model->disapproval_description ?? "در انتظار تأیید";
            return '<i class="far fa-times-circle text-warning font-size-150p" data-toggle="tooltip" title="' . $title . '"></i>';
    }

}

function toJson(array $array) {
    return response()->json($array);
}

function jsonResponseTrue() {
    return toJson(['status' => true]);
}

