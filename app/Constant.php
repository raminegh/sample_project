<?php


namespace App;


class Constant
{
    const ARTICLE_TYPE = 'article';
    const ALBUM_TYPE = 'album';
    const VIDEO_TYPE = 'video';
    const IMAGE_TYPE = 'image';
    const NEWS_TYPE = 'news';
    const USER_TYPE = 'user';
    const MESSAGE_TYPE = 'message';
    const COOPERATION_TYPE = 'cooperation';
    const EMPLOYMENT_TYPE = 'employment';
    const DISCOUNT_TYPE = 'discount';
    const SALON_TYPE = 'salon';
    const CATEGORY_TYPE = 'category';
    const TAG_TYPE = 'tag';
    const GROUP_TYPE = 'group';
    const ROLE_TYPE = 'role';
    const BUSINESS_TYPE = 'business';


    const MODEL_TYPE_ARRAY = [
        self::ARTICLE_TYPE,
        self::NEWS_TYPE,
        self::VIDEO_TYPE,
        self::IMAGE_TYPE,
    ];

    const SEARCH_TYPE_ARRAY = [
        self::ARTICLE_TYPE,
        self::NEWS_TYPE,
        self::VIDEO_TYPE,
        self::IMAGE_TYPE,
        self::EMPLOYMENT_TYPE,
        self::DISCOUNT_TYPE,

    ];

    const VIDEO_COVER_FOLDER = 'video_cover';

    const TITLE_COOPERATION_NOTIF = " برای شما درخواست همکاری فرستاده است.";
    const TITLE_FOLLOW_NOTIF = " شما را دنبال کرد.";
    const TITLE_LIKE_NOTIF = " شما را لایک کرد.";
    const TITLE_COMMENT_NOTIF = " شما نظر داده است.";
    const TITLE_MENTION_NOTIF = " رجوع داده است.";
    const TITLE_REPLY_NOTIF = " به نظر شما پاسخی داده است.";
    const TITLE_PERSONNEL_NOTIF = " درخواست همکاری شما را پذیرفته است.";
    const TITLE_MESSAGE_NOTIF = " برای شما پیام فرستاده است.";

    const USER_DEFAULT_AVATAR = '/assets/avatar/default-avatar.png';
    const SALON_DEFAULT_AVATAR = '/assets/avatar/default-salon-avatar.png';

    const JALALI_DATE_REGEX_VALIDATE = "/^[1-4]\d{3}\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))$/";


    const CACHE_MAIN_SLIDES = 'main_slide';
    const CACHE_TOP_SALONS = 'top_salons';
    const CACHE_ARTICLES_HOME_PAGE = 'articles_home_page';
    const CACHE_NEWS_HOME_PAGE = 'news_home_page';
    const CACHE_SERVICES_HOME_PAGE = 'services_home_page';
    const CACHE_FOOTER_DATA = 'footer_data';
    const CACHE_APP_GENERAL_INFO = 'app_general_info';
    const CACHE_CLICK_AD_IP = 'click_ad_ip_';
    const CACHE_VIEW_AD_IP = 'view_ad_ip_';
    const CACHE_NAME_USER = 'name_user_';
    const CACHE_AVATAR_USER = 'avatar_user_';
    const CACHE_PERMISSIONS = 'permissions';
    const CACHE_ROLE_USER = 'role_user_';
    const CACHE_ROLES_PERMISSION = 'roles_permissionn_';
    const CACHE_CATEGORIES_ARTICLE = 'categories_article';
    const CACHE_CATEGORIES_NEWS = 'categories_news';
    const CACHE_CATEGORIES_MEDIA = 'categories_media';

    const ONE_MINUTE_SECONDS = 60;
    const TWO_MINUTE_SECONDS = 120;
    const THREE_MINUTE_SECONDS = 180;
    const FIVE_MINUTE_SECONDS = 300;
    const TEN_MINUTE_SECONDS = 600;
    const FIFTEEN_MINUTE_SECONDS = 900;
    const THIRTY_MINUTE_SECONDS = 1800;
    const ONE_HOUR_SECONDS = 3600;
    const TWO_HOUR_SECONDS = 7200;
    const TWELVE_HOUR_SECONDS = 43200;
    const ONE_DAY_SECONDS = 86400;


    const INVALID_USERNAMES = [
        'user',
        'users',
        'video',
        'videos',
        'app',
        'apps',
        'admin',
        'admins',
        'root',
        'roots',
        'album',
        'albums',
        'logout',
        'login',
        'register',
        'signup',
        'log',
        'logs',
        'message',
        'messages',
        'auth',
        'home',
        'main',
        'dashboard',
        'dashboards',
        'profile',
        'profiles',
        'personnel',
        'personnels',
        'comment',
        'comments',
        'comments',
        'business',
        'businesses',
        'businesss',
        'service',
        'services',
        'employment',
        'employments',
        'discount',
        'discounts',
        'article',
        'articles',
        'new',
        'news',
        'name',
        'setting',
        'settings',
        'editor-upload-image',
        'job',
        'jobs',
        'salon',
        'salons',
        'position',
        'positions',
        'image',
        'images',
        'like',
        'likes',
        'tag',
        'tags',
        'cooperation',
        'cooperations',
        'notification',
        'notifications',
        'category',
        'categories',
        'group',
        'groups',
        'access',
        'ad',
        'ads',
        'statistics',
        'statistic',
        'place',
        'places',
        'location',
        'map',
        'maps',
        'email',
        'gmail',
        'send',
        'about',
        'about-us',
        'aboutus',
        'contact',
        'contact-us',
        'contactus',
        'test',
        'tests',
        'post',
        'posts',
        'book',
        'books',
        'shop',
        'shops',
        'store',
        'list',
        'club',
        'film',
        'clip',
        'song',
        'mp3',
        'mp4',
        'person',
        'guest',
        'log-out',
        'log-in',
        'blog',
        'blogs',
        'show',
        'upload',
        'exit',
        'bio',
        'web',
        'save',
        'share',
        'site',
        'website',
        'sitemap',
        'sitemaps',
    ];

    const ADMIN_LIST_COUNT = 12;
    const APP_LIST_COUNT = 12;

}
