<?php

namespace App\Listeners\UploadVideo;

use App\Events\UploadNewVideo;
use App\Jobs\ConvertAndWaterMarkToUploadedVideoJob;
use App\Jobs\ProcessVideoForDownloadJob;
use App\Models\Video;
use Carbon\Carbon;
use FFMpeg\Filters\Video\CustomFilter;
use FFMpeg\Format\Video\X264;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Pbmedia\LaravelFFMpeg\FFMpegFacade;
use Pbmedia\LaravelFFMpeg\Media;

class ProcessUploadedVideo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UploadNewVideo  $event
     * @return void
     */
    public function handle(UploadNewVideo $event)
    {
        ConvertAndWaterMarkToUploadedVideoJob::dispatch($event->getVideo(), $event->getTmpPath(), $event->getFinalPath(), $event->getFilename(), $event->getWatermark());
        ProcessVideoForDownloadJob::dispatch($event->getVideo(), $event->getTmpPath(), $event->getFinalPath(), $event->getFilename(), $event->getWatermark());
    }
}
