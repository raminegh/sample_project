<?php

namespace App\Listeners\UserRegistered;

use App\Events\UserRegistered;
use App\Facades\SMSFacade;
use App\Settings;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendSMSCodeVerification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $user = $event->user;
        $authCode = $user->generateVerificationCode($event->hash);
        if($authCode) {
            $text = get_setting(Settings::AUTH_SMS_TEXT)  . "\r\n" . $authCode ?? '';
            $SMS = new SMSFacade();
            $SMS->send($user->phone, $text);
        }
    }
}
