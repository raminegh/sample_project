<?php

namespace App;

use App\Events\UserRegistered;
use App\models\Ads;
use App\Models\Album;
use App\Models\Group;
use App\Models\Image;
use App\Models\Notification;
use App\Models\Personnel;
use App\Models\Position;
use App\Models\Message;
use App\Models\PositionRequest;
use App\Models\Profile;
use App\Models\Cooperation;
use App\Models\Salon;
use App\Models\UserAdditionalInformation;
use App\Models\VerificationCode;
use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRole;

    const UNKNOWN = 0;
    const ADMIN = 1;
    const BEAUTY_ACTIVIST = 2;
    const BUSINESS_OWNER = 3;
    const USER = 4;

    const LEVELS = [
        self::UNKNOWN,
        self::USER,
        self::BEAUTY_ACTIVIST,
        self::BUSINESS_OWNER,
        self::ADMIN
    ];

    protected $guarded = ['id'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeSearch($query, $keywords)
    {
        $keywords = explode(' ', $keywords);
        $query->where('level', User::BEAUTY_ACTIVIST)->orWhere('level', User::USER);
        foreach ($keywords as $keyword) {
            $query->where(function ($query) use ($keyword) {
                $query->where('name', 'LIKE', '%' . $keyword . '%')->orWhere('username', 'LIKE', '%' . $keyword . '%');
            });
        }
        return $query;
    }

    public function scopeGetAll($query)
    {
        return $query->orderBy('id', 'DESC')->whereIn('level', [User::BEAUTY_ACTIVIST, User::USER]);
    }

    public function generateVerificationCode($hash)
    {
        if (count($this->verificationCodes) < 2) {
            $code = rand(100000, 999999);
            VerificationCode::create([
                'code' => $code,
                'user_id' => $this->id,
                'expire_at' => now()->addMinutes(10),
                'hash' => $hash
            ]);
            return $code;
        }

        return false;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getIsActiveLabel()
    {
        if ($this->attributes['is_active'] == 1) {
            return "فعال";
        }
        return "غیرفعال";
    }

    public function setPhoneAttribute($value)
    {
        $phone = '+98' . substr($value, -10, 10);
        $this->attributes['phone'] = $phone;
    }


    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }

    public function getIsActiveBtn()
    {
        if ($this->attributes['is_active'] == 1) {
            return "btn btn-secondary";
        }
        return "btn btn-primary";
    }

    public function getLevel()
    {
        switch ($this->attributes['level']) {
            case self::ADMIN:
                return "مدیر";
            case self::USER:
                return "علاقه مند به حوزه زیبایی";
            case self::BEAUTY_ACTIVIST:
                return "فعال زیبایی";
            case self::BUSINESS_OWNER:
                return "صاحب کسب و کار";
            default:
                return "نامشخص";
        }
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getIsActiveTitle()
    {
        if ($this->attributes['is_active'] == 1) {
            return "غیرفعال";
        }
        return "فعال";
    }

    public function isAdmin()
    {
        return $this->level == self::ADMIN ? true : false;
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'user_id', 'id')->where('deleted_for', null)->orWhere('deleted_for', '=', Message::DELETED_FOR_RECEIVER);
    }

    public function receivedMessages()
    {
        return $this->hasMany(Message::class, 'receiver_id', 'id')->where('deleted_for', null)->orWhere('deleted_for', '=', Message::DELETED_FOR_SENDER);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_users');
    }

    public function salon()
    {
        return $this->hasOne(Salon::class);
    }

    public function verificationCodes()
    {
        return $this->hasMany(VerificationCode::class)->where('is_used', '0')->where('expire_at', '>', now())->latest();
    }

    public function getValidHash()
    {
        return $this->hasMany(VerificationCode::class)->where('is_used', '0')->where('expire_at', '>', now())->pluck('hash')->first();
    }

    public function positions()
    {
        return $this->belongsToMany(Position::class);
    }


    public function stores()
    {
        return $this->hasManyThrough(Store::class, Area::class);
    }

    public function positionRequests()
    {
        return $this->hasMany(PositionRequest::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function albums()
    {
        return $this->hasMany(Album::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function approvedImages()
    {
        return $this->hasMany(Image::class)->where('is_approved', 1);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function approvedVideos()
    {
        $date = Carbon::now()->toDateString();
        return $this->hasMany(Video::class)
            ->where('hls_state', Video::ACCEPTED_STATE)
            ->whereDate('publish_at','<=', $date);
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'user_followers', 'user_id', 'follower_id');
    }

    public function followings()
    {
        return $this->belongsToMany(User::class, 'user_followings', 'user_id', 'following_id');
    }

    public function receiveCooperations()
    {
        return $this->hasMany(Cooperation::class, 'receiver_id');
    }

    public function sendCooperations()
    {
        return $this->hasMany(Cooperation::class, 'sender_id');
    }

    public function personnel()
    {
        return $this->hasMany(Personnel::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'owner_id')
            ->where('created_at', '>', now()->subDays(30))->orderBy('id', 'desc');
    }

    public function ads()
    {
        return $this->hasMany(Ads::class);
    }

    public function scopeUsername($query, $username)
    {
        return $query->whereUsername($username)->firstOrFail();
    }


}
