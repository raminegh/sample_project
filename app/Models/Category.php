<?php

namespace App;

use App\models\Ads;
use App\Models\Discount;
use App\Models\Employment;
use App\Models\Image;
use App\Models\Video;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = ['name', 'label', 'parent_id', 'type'];

	const TYEP_DEFAULT = 0;
	const TYEP_ARTICLE = 1;
	const TYEP_NEWS = 2;
	const TYEP_MEDIA = 3;

	public function parent()
	{
		$parent = $this->belongsTo(Category::class, 'parent_id', 'id')->get()->pluck('name')->toArray();
		return sizeof($parent) > 0 ? $parent[0] : '-';
	}

    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id');
	}

	public function label()
	{
		return $this->attributes['label'] ? $this->attributes['label'] : '-';
	}

    public function articles()
    {
        return $this->morphedByMany(Article::class, 'categoriable', 'category_items');
    }

    public function news()
    {
        return $this->morphedByMany(News::class, 'categoriable', 'category_items');
    }

    public function videos()
    {
        return $this->morphedByMany(Video::class, 'categoriable', 'category_items');
    }

    public function images()
    {
        return $this->morphedByMany(Image::class, 'categoriable', 'category_items');
    }


    public function discounts()
    {
        return $this->morphedByMany(Discount::class, 'categoriable', 'category_items')
            ->where('is_approved', 1)
            ->where('is_valid', 1)
            ->latest();
    }

    public function employments()
    {
        return $this->morphedByMany(Employment::class, 'categoriable', 'category_items')
            ->where('is_approved', 1)
            ->where('is_valid', 1)
            ->latest();
    }

    public function ads()
    {
        return $this->morphedByMany(Ads::class, 'categoriable', 'category_items');
    }

}
