<?php

namespace App;

use App\Models\Discount;
use App\Models\Employment;
use App\Models\Video;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = ['id'];

    public function taggable()
    {
        return $this->morphTo();
    }

    public function articles()
    {
        return $this->morphedByMany(Article::class, 'taggable');
    }

    public function news()
    {
        return $this->morphedByMany(News::class, 'taggable');
    }

    public function videos()
    {
        return $this->morphedByMany(Video::class, 'taggable');
    }

    public function employments()
    {
        return $this->morphedByMany(Employment::class, 'taggable')
            ->where('is_approved', 1)
            ->where('is_valid', 1)
            ->latest();
    }

    public function discounts()
    {
        return $this->morphedByMany(Discount::class, 'taggable')
            ->where('is_approved', 1)
            ->where('is_valid', 1)
            ->latest();
    }



}
