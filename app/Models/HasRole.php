<?php
/**
 * Created by PhpStorm.
 * User: RamiN
 * Date: 20/09/2019
 * Time: 06:26 AM
 */

namespace App;


trait HasRole
{
	public function roles()
	{
        return $this->belongsToMany(Role::class);
	}

	public function hasRole($role)
	{
		if(is_string($role)) {
		    \Cache::forget(Constant::CACHE_ROLE_USER . $role . '_' . auth()->id());
            return \Cache::remember(Constant::CACHE_ROLE_USER . $role . '_' . auth()->id(), Constant::THIRTY_MINUTE_SECONDS, function () use ($role){
                return $this->roles->contains('name', $role);
            });
		}

		foreach ($role as $r){
            if($this->hasRole($r->name)) {
                return true;
            }
		}

		return false;
	}
}
