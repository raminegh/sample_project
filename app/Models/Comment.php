<?php

namespace App;

use App\Models\Video;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $fillable = [
		'user_id',
		'name',
		'parent_id',
		'comment',
		'commentable_id',
		'approved',
		'commentable_type',
		'email'
	];

	public function commentable()
	{
		return $this->morphTo();
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function article()
	{
		return $this->belongsTo(Article::class, 'commentable_id', 'id');
	}

	public function new()
	{
		return $this->belongsTo(News::class, 'commentable_id', 'id');
	}

    public function video()
    {
        return $this->belongsTo(Video::class, 'commentable_id', 'id');
    }


	public function comments()
	{
		return $this->hasMany(Comment::class , 'parent_id' , 'id')->where('approved', 1);
	}

	public function setCommentAttribute($value)
	{
		$this->attributes['comment'] = nl2br($value);
	}


    public function approvedInfo()
    {
        if($this->attributes['approved'] == 0) {
            return ['icon' => 'far fa-eye', 'title' => 'نمایش داده شود', 'color' => 'text-success'];
        }
        return ['icon' => 'far fa-eye-slash', 'title' => 'نمایش داده نشود', 'color' => 'text-warning'];
	}


}
