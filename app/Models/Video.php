<?php

namespace App\Models;

use App\Category;
use App\Comment;
use App\Like;
use App\Tag;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'video_id',
        'user_id',
        'title',
        'description',
        'cover',
        'subtitles',
        'videoable_id',
        'videoable_type',
        'hls_state',
        'download_state'
    ];

    protected $casts = [
        'cover' => 'array',
        'download_url' => 'array',
        'hls_url' => 'array',
        'subtitles' => 'array'
    ];

    public function getRouteKeyName()
    {
        return 'video_id';
    }

    const PENDING_STATE = 'pending';
    const CONVERTED_STATE = 'converted';
    const ACCEPTED_STATE = 'accepted';
    const BLOCKED_STATE = 'blocked';

    const STATE_ARRAY = [
        self::PENDING_STATE,
        self::CONVERTED_STATE,
        self::ACCEPTED_STATE,
        self::BLOCKED_STATE
    ];

    public function scopeSearch($query, $keywords)
    {
        $keywords = explode(' ', $keywords);
        foreach ($keywords as $keyword) {
            $query->whereHas('categories', function ($query) use ($keyword){
                $query->where('name', 'LIKE', '%' . $keyword . '%');
            })
                ->orWhereHas('tags', function ($query) use ($keyword){
                    $query->where('name', 'LIKE', '%' . $keyword . '%');
                })
                ->orWhere('title', 'LIKE', '%' . $keyword . '%', function ($query){
                    $query->where('hls_state', Video::ACCEPTED_STATE);
                });
        }
        return $query;
    }

    public function scopeGetAll($query) {
        return $query->orderBy('id', 'desc')->where('hls_state', self::ACCEPTED_STATE);
    }

    public function videoable()
    {
        return $this->morphTo();
    }

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categoriable', 'category_items');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function approvedComments()
    {
        return $this->morphMany(Comment::class, 'commentable')->where('approved', 1)->where('parent_id', 0)->latest()->with(['comments' => function($query) {
            $query->where('approved', 1)->oldest();
        }]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function viewsCount()
    {
        return $this->hasMany(VideoView::class);
    }

    public function getDurationAttribute($duration)
    {
        $minutes = floor($duration / 60);
        if($minutes < 10) {
            $minutes = '0' . $minutes;
        }
        $seconds = $duration % 60;
        return $minutes . ':' . $seconds;
    }


}
