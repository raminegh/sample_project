<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /* general info about site */
    const APP_GENERAL_INFO = 'app_general_info';
    const APP_NAME = 'app_name';
    const APP_LOGO = 'app_logo';
    const APP_SLOGAN = 'app_slogan';

    const AUTH_SMS_TEXT = 'auth_sms_text';

    const SLIDE_MAIN_PAGE = 'slide_main_page';

    const FOOTER_DATA = 'footer_data';
    const FOOTER_SOCIAL_MEDIA = 'footer_social_media';
    const FOOTER_COL_TWO = 'footer_col_two';
    const FOOTER_COL_THREE = 'footer_col_three';
    const FOOTER_SIGNS = 'footer_signs';

    const FOOTER_SIGN_IMAGE = 'image';
    const FOOTER_SIGN_LINK = 'link';


    const FOOTER_ITEM_ONE_LINK = 'item_one_link';
    const FOOTER_ITEM_TWO_LINK = 'item_two_link';
    const FOOTER_ITEM_THREE_LINK = 'item_three_link';
    const FOOTER_ITEM_FOUR_LINK = 'item_four_link';


    const FOOTER_COL_TITLE = 'title';
    const FOOTER_COL_ITEM_ONE = 'item_one';
    const FOOTER_COL_ITEM_TWO = 'item_two';
    const FOOTER_COL_ITEM_THREE = 'item_three';
    const FOOTER_COL_ITEM_FOUR = 'item_four';

    const CONTACT_US = "contact_us";

    protected $guarded = ['id'];

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = str_replace(PHP_EOL, "<br/>", $value);
    }
}
