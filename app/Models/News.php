<?php

namespace App;

use App\Models\View;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    use Sluggable;
    protected $guarded = ['id'];

    protected $casts = [
        'cover' => 'array'
    ];


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function path() {
        return "/news/$this->slug";
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeSearch($query, $keywords)
    {
        $keywords = explode(' ', $keywords);
        foreach ($keywords as $keyword) {
            $query->whereHas('categories', function ($query) use ($keyword){
                $query->where('name', 'LIKE', '%' . $keyword . '%');
            })
                ->orWhereHas('tags', function ($query) use ($keyword){
                    $query->where('name', 'LIKE', '%' . $keyword . '%');
                })
                ->orWhere('title', 'LIKE', '%' . $keyword . '%');
        }
        return $query;
    }


    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function relateds()
    {
        $news = $this->categories()->first()->news;
        if ($news->count() > 2) {
            $news = $news->random(3);
        } else if ($news->count() > 1) {
            $news = $news->random(2);
        }
        return $news;
    }

    public function approvedComments()
    {
        return $this->morphMany(Comment::class, 'commentable')->where('approved', 1)->where('parent_id', 0)->latest()->with(['comments' => function($query) {
            $query->where('approved', 1)->oldest();
        }]);
    }

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categoriable', 'category_items');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function views()
    {
        return $this->morphMany(View::class, 'viewable');
    }

}
