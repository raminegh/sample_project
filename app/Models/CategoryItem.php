<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryItem extends Model
{
    protected $fillable = ['category_id', 'categoriable_id', 'categoriable_type'];

    public $timestamps = false;
}
