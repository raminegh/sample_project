window.profileJs = () => {
    $('body').on('change', '#file-input', function (e) {
        let form = new FormData(document.getElementById('upload-avatar-form'));
        let avatar = e.target.files[0];
        form.append('avatar', avatar);
        $.ajax({
            url: '/admin/profile/set-avatar',
            type: 'POST',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form,
            success: function (data) {
                $('#profile-avatar').attr('src', data);
                $('#file-input').val('');
            },
            error: (error) => ajaxError(error)
        })
    });
};

// category  --------------------------------------------<
window.categoryJs = function () {

    const categoriesLoad = () => {
        const types = [1, 2, 3];

        types.forEach((type) => {
            switch (type) {
                case 1:
                    load("#article", '/admin/category/list?type=' + type);
                    break;
                case 2:
                    load("#news", '/admin/category/list?type=' + type);
                    break;
                case 3:
                    load("#media", '/admin/category/list?type=' + type);
                    break;
            }
        });
    };


    categoriesLoad();

    window.store = function () {
        const name = $('#name').val();
        const label = $('#label').val();
        const parent_id = $('#parent-id').val();
        const type = $('#type').val();
        $.ajax({
            url: '/admin/category/store',
            type: 'POST',
            dataType: 'json',
            data: {
                name,
                label,
                parent_id,
                type
            },
            success: function (data) {
                if (data.status) {
                    $('#add-category-modal').modal('hide');
                    categoriesLoad();
                } else {
                    categoriesLoad();
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    window.edit = function (id) {
        const name = $('#name').val();
        const label = $('#label').val();
        const parent_id = $('#parent-id').val();
        $.ajax({
            url: '/admin/category/update',
            type: 'POST',
            dataType: 'json',
            data: {
                id,
                name,
                label,
                parent_id
            },
            success: function (data) {
                if (data.status) {
                    $('#add-category-modal').modal('hide');
                    load(getNumberPagination());
                } else {
                    load(getNumberPagination());
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    function resetModal() {
        $('#name').val('');
        $('#label').val('');
        $("#parent_id option[value='']").prop('selected', true);
        $("#type option[value='']").prop('selected', true);
    }

    $('body').on('click', '.add-btn', function () {
        $('.only-show-in-store').show();
        $('#isEdit').val('false');
        $('.modal-title').text('ایجاد دسته بندی');
        $('#store').text('ایجاد');
        resetModal();
    });

    $('body').on('click', '#store', function () {
        $('#isEdit').val() == 'true' ? edit($("#category-id").val()) : store();
    });

    $('body').on('change', '#type', function () {
        getCategoriesOptions($(this).val());
    });

    $('body').on('click', '.edit-btn', function () {
        const jElement = $(this);
        getOptions(jElement.data('type'));
        $('.only-show-in-store').hide();
        $('#isEdit').val('true');
        resetModal();
        $('.modal-title').text('ویرایش دسته بندی');
        $('#store').text('اعمال تغییرات');
        $("#category-id").val(jElement.data('category-id'));
        const parent_id = jElement.data('parent-id');
        const label = jElement.data('label') ?? '';
        const name = jElement.data('name');
        $('#name').val(name);
        $('#label').val(label);
        $("#parent-id").val(parent_id).trigger('change');
    });

};

// tag  --------------------------------------------<
window.tagJs = function () {
    load();

    window.store = function () {
        let name = $('#name').val();
        $.ajax({
            url: '/admin/tag/store',
            type: 'POST',
            dataType: 'json',
            data: {
                name,
            },
            success: function (data) {
                if (data.status) {
                    $('#add-tag-modal').modal('hide');
                    load();
                } else {
                    load(getNumberPagination());
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    window.edit = function (id) {
        let name = $('#name').val();
        $.ajax({
            url: '/admin/tag/update',
            type: 'POST',
            dataType: 'json',
            data: {
                id,
                name,
            },
            success: function (data) {
                if (data.status) {
                    $('#add-tag-modal').modal('hide');
                    load()
                } else {
                    load()
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    function resetModal() {
        $('#name').val('');
    }

    $('body').on('click', '.add-btn', function () {
        $('#isEdit').val('false');
        $('.modal-title').text('ایجاد تگ');
        $('#store').text('ایجاد');
        resetModal();
    });

    $('body').on('click', '#store', function () {
        $('#isEdit').val() == 'true' ? edit($("#tag-id").val()) : store();
    });

    $('body').on('click', '.edit-btn', function () {
        $('#isEdit').val('true');
        resetModal();
        $('.modal-title').text('ویرایش تگ');
        $('#store').text('اعمال تغییرات');
        parent = $(this).parent().parent();
        $("#tag-id").val(parent.data('id'));
        let name = parent.find('.name').text();
        $('#name').val(name);
    });

};

// users  --------------------------------------------------<
window.userJs = function () {
    load();

    window.update = function () {
        let id = $('#input-id').val();
        let name = $('#name').val();
        let username = $('#username').val();

        $.ajax({
            url: '/admin/user/update',
            type: 'POST',
            dataType: 'json',
            data: {
                id,
                name,
                username
            },
            success: function (data) {
                if (data.status) {
                    $('#edit-modal').modal('hide');
                    load()
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    // edit user

    $('body').on('click', '.edit-btn', function () {
        parent = $(this).parent().parent().parent();
        let name = parent.find('.name').data('name');
        let username = parent.find('#username-input').val();
        let id = parent.data('user-id');
        $('#input-id').val(id);
        $('#name').val(name);
        $('#username').val(username);
    });

    $('body').on('click', '.update-btn', function () {
        update();
    });

    $('body').on('click', '#select-user-for-refer-btn', function () {
        $('#user-for-refer-input').val($(this).data('user-id'));
    });

    $('body').on('click', '#refer-to-salon-btn', function () {
        let salon_id = $('#salon-select').val();
        let user_id = $('#user-for-refer-input').val();
        $.ajax({
            url: '/admin/user/refer-to-salon',
            type: 'post',
            dataType: 'json',
            data: {
                salon_id,
                user_id
            },
            success: function (data) {
                if (data.status) {
                    $('#refer-to-salon-modal').modal('hide');
                }
            },
            error: (error) => ajaxError(error)
        })
    });

};


window.managerialUserJs = function () {
    load();

    // delete User access
    window.deleteAccess = function (id) {

        $.ajax({
            url: '/admin/access/delete-access',
            type: 'POST',
            dataType: 'json',
            data: {
                id
            },
            success: function (data) {
                if (data.status) {
                    load()
                } else {
                    Swal.fire({
                        title: "خطا",
                        text: "عملیات حذف دسترسی ها ناموفق بود",
                        confirmButtonText: "تایید"
                    });
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    $('body').on('click', '.delete-access-btn', function () {
        const user_id = $(this).parent().parent().parent().data('user-id');
        deleteAccess(user_id);
    })

};

// roles  --------------------------------------------------<
window.roleJs = function () {
    load();

    window.getPermissions = function (id) {
        $.ajax({
            url: '/admin/access/role/get-permissions/' + id,
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $("#permissions-in-modal").empty();
                if (data.length > 0) {
                    $("#permissions-in-modal").append(data);
                } else {
                    $("#permissions-in-modal").append("موردی یافت نشد.");
                }
            },
            error: (error) => ajaxError(error)
        });

    };

    $('body').on('change', '.permission-parent-checkbox', function () {
        let targetId = $(this).val();
        let target = $('.parent-' + targetId);
        if (!$(this).prop('checked')) {
            target.find('input[type=checkbox]:checked').prop('checked', false)
        }
        target.slideToggle();
    });

    $('body').on('click', '#show-permissions-btn', function () {
        $('#show-permissions-modal').modal('show');
        let role = $(this).data('role');
        $('#name-in-modal').text(role.name);
        $('#label-in-modal').text(role.label);
        getPermissions(role.id);

    });

};

window.adsCreateJs = function () {
    window.loadSelectedField = (type) => {
        $.ajax({
            url: `/admin/ads/get-type-form/${type}`,
            type: 'POST',
            dataType: 'html',
            success: data => {
                $('#ad-form-container').empty();
                $('#ad-form-container').append(data);
                $('#save-ad-btn').prop('disabled', false);
                if (+type === 3) {
                    $('.dropzone-area').show();
                } else {
                    $('.dropzone-area').hide();
                }
            },
            error: (error) => ajaxError(error)
        });
    };
    $('body').on('change', '#type', function () {
        const type = $(this).find('option:selected').val();
        loadSelectedField(type);
    });

};


// comments ------------------------------------------------<
window.commentJs = function () {

    const types = ["article", "news", "video", 'image'];

    types.forEach((type) => {
        switch (type) {
            case types[0]:
                load("#cm-articles", '/admin/comment/list?type=' + type);
                break;
            case types[1]:
                load("#cm-news", '/admin/comment/list?type=' + type);
                break;
            case types[2]:
                load("#cm-videos", '/admin/comment/list?type=' + type);
                break;
            case types[3]:
                load("#cm-images", '/admin/comment/list?type=' + type);
                break;
        }
    });


    window.toggleApproved = function (sender) {
        const approved = sender.data('approved') == 0 ? 1 : 0;
        sender.data('approved', approved);
        const comment_id = sender.parent().data('comment-id');
        $.ajax({
            url: '/admin/comment/toggle-approved',
            type: 'POST',
            dataType: 'json',
            data: {
                comment_id: comment_id,
                approved: approved,
            },
            success: function (data) {
                if (data.status) {
                    sender.find('i').removeClass();
                    if (+approved === 1) {
                        sender.find('i').addClass('far fa-eye-slash');
                        sender.removeClass('text-success');
                        sender.addClass('text-warning');
                        sender.attr('title', 'نمایش داده نشود');
                    } else {
                        sender.find('i').addClass('far fa-eye');
                        sender.removeClass('text-warning');
                        sender.addClass('text-success');
                        sender.attr('title', 'نمایش داده شود');
                    }
                } else {
                    alert("عملیات انجام نشد");
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    window.reply = function (sender) {
        const parent_id = $('#parent-id').val();
        const commentable_id = $('#commentable-id').val();
        const commentable_type = $('#commentable-type').val();
        const comment_id = $('#comment-id').val();
        const comment = $('#comment').val();
        $.ajax({
            url: '/admin/comment/add-comment',
            type: 'POST',
            dataType: 'json',
            data: {
                comment_id: comment_id,
                parent_id: parent_id,
                commentable_id: commentable_id,
                commentable_type: commentable_type,
                comment: comment
            },
            success: function (data) {
                if (data.status) {
                    let btn = sender.parent();
                    let textarea = btn.prev();
                    btn.slideUp('slow', function () {
                        btn.remove();
                    });
                    textarea.slideUp('slow', function () {
                        textarea.remove();
                    });
                    load();
                } else {
                    alert("عملیات انجام نشد");
                }
            },
            error: (error) => ajaxError(error)

        });
    };

    $('body').on('click', '.toggle-approved-btn', function () {
        toggleApproved($(this));
    });

    $('body').on('click', '.reply-btn', function () {
        $('#save-reply-btn').parent().remove();
        $('#comment-text').parent().remove();
        const comment_id = $(this).parent().data('comment-id');
        const parent_id = $(this).parent().data('parent-id');
        const commentable_id = $(this).parent().data('commentable-id');
        const commentable_type = $(this).parent().data('commentable-type');
        $('#parent-id').val(parent_id);
        $('#commentable-id').val(commentable_id);
        $('#commentable-type').val(commentable_type);
        $('#comment-id').val(comment_id);
        let textarea = "<div class='form-group'><textarea id='comment-text' class='form-control' placeholder='پاسخ خود را بنویسید'></textarea></div> <div class='form-group text-left'><button id='save-reply-btn' class='btn btn-outline-primary prl32'>ارســال</button></div>";
        $(this).parent().parent().next().next().next().append(textarea).slideDown('slow');
    });

    $('body').on('click', '#save-reply-btn', function () {
        const comment = $('#comment-text').val();
        $('#comment').val(comment);
        reply($(this));
    });

};


// messages -------------------------------------------------<
window.messageJs = function () {
    load();

    window.send = function () {
        let name = $('#name').val();
        let receiverId = $('#receiver-id').find(':selected').val();
        let type = $('#type').find(':selected').val();
        let text = $('#text').val();
        $.ajax({
            url: '/admin/message/send',
            type: 'POST',
            dataType: 'json',
            data: {
                name: name,
                receiverId: receiverId,
                type: type,
                text: text
            },
            success: function (data) {
                if (data.status) {
                    $('#send-message-modal').modal('hide');
                    $('#alert-message').removeClass();
                    $('#alert-message').addClass('alert alert-success lert-dismissible fade show');
                    $('#name').val('');
                    $('#text').val('');
                    $('#type').prop('selectedIndex', 0);
                    $("#receiver-id").html("<option value=''></option>");
                    $('#alert-message').show();
                    $('#message').text(data.message);
                } else {
                    dismisAlert('error', data.message);
                }
            }, error: (error) => ajaxError(error)

        })
    };

    // edit user
    $('body').on('click', '#send', function () {
        send()
    });

};

// groups ----------------------------------------------------<
window.groupJs = function () {

    load();

    window.members = function () {
        let groupId = $('#group-id').val();
        $.ajax({
            url: '/admin/group/members',
            type: 'POST',
            dataType: 'html',
            data: {
                group_id: groupId
            },
            success: function (data) {
                $("#modal-container-ajax").empty();
                if (data.length > 0) {
                    $('#modal-container-ajax').append(data)
                } else {
                    $('#modal-container-ajax').append("موردی یافت نشد")
                }
            },
            error: (error) => ajaxError(error)
        })
    };

    $('body').on('click', '.members-btn', function () {
        $('#group-id').val($(this).data('group-id'));
        members();
    })

};

// business --------------------------------------------------<
window.businessJs = function () {
    load();

    window.addBusiness = function () {
        let name = $('#name').val();
        let description = $('#description').val();
        let category_id = $('#category_id').find(":selected").val();
        $.ajax({
            url: '/admin/business/create',
            type: 'POST',
            dataType: 'html',
            data: {
                name,
                description,
                category_id
            },
            success: function (data) {
                $('#business').empty();
                $('#business').append(data);
                $('.service-container').slideDown(400);
                $('.job-container').slideDown(800);
            },
            error: (error) => ajaxError(error)
        });
    };

    window.addService = function () {
        let business_id = $('#business-id').val();
        let name = $('#service-name').val();
        let description = $('#service-description').val();
        $.ajax({
            url: '/admin/service/create',
            type: 'POST',
            dataType: 'json',
            data: {
                business_id,
                name,
                description,
            },
            success: function (data) {
                if (data.status) {
                    load('#services', '/admin/service/list/' + business_id);
                } else {
                    console.log(data.status);
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    window.editBusiness = function () {
        let id = $('#business-id').val();
        let name = $('#name').val();
        let description = $('#description').val();
        let categories = $('#category_id').val();
        $.ajax({
            url: '/admin/business/edit/' + id,
            type: 'POST',
            dataType: 'html',
            data: {
                name,
                description,
                categories
            },
            success: function (data) {
                $('#business').empty();
                $('#business').append(data);
            },
            error: (error) => ajaxError(error)
        });
    };

    window.editService = function () {
        let id = $('#id').val();
        let name = $('#name-edit').val();
        let description = $('#description-edit').val();
        $.ajax({
            url: '/admin/service/edit/' + id,
            type: 'POST',
            dataType: 'json',
            data: {
                name,
                description,
            },
            success: function (data) {
                if (data.status) {
                    $('#edit-modal').modal('hide');
                    services();
                } else {
                    console.log(data.status);
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    window.editJob = function () {
        let id = $('#id').val();
        let name = $('#name-edit').val();
        let description = $('#description-edit').val();
        $.ajax({
            url: '/admin/position/edit/' + id,
            type: 'POST',
            dataType: 'json',
            data: {
                name,
                description,
            },
            success: function (data) {
                if (data.status) {
                    $('#edit-modal').modal('hide');
                    jobs();
                } else {
                    console.log(data.status);
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    window.addJob = function () {
        let business_id = $('#business-id').val();
        let name = $('#job-name').val();
        let description = $('#job-description').val();
        $.ajax({
            url: '/admin/position/create',
            type: 'POST',
            dataType: 'json',
            data: {
                business_id,
                name,
                description,
            },
            success: function (data) {
                if (data.status) {
                    load("#jobs", '/admin/position/list/' + business_id);
                }
            },
            error: (error) => ajaxError(error)
        });
    };

    function operationOfEdit(sender, type) {
        let id = sender.data('id');
        let name = sender.parent().prev().prev().text();
        let description = sender.parent().prev().text();
        $('#id').val(id);
        $('#name-edit').val(name);
        $('#description-edit').val(description);
        $('#edit-modal').modal('show');
        $('#type').val(type);
    }

    $('body').on('click', '#add-business-btn', function () {
        addBusiness();
    });

    $('body').on('click', '#add-service-btn', function () {
        addService()
    });

    $('body').on('click', '#add-job-btn', function () {
        addJob()
    });

    $('body').on('click', '#save-edit-btn', function () {
        let type = $('#type').val();
        switch (type) {
            case 'service':
                editService();
                break;
            case 'job':
                editJob();
                break;
        }
    });

    $('body').on('click', '.edit-service-btn', function () {
        operationOfEdit($(this), 'service')
    });

    $('body').on('click', '.edit-job-btn', function () {
        operationOfEdit($(this), 'job')
    });

    $('body').on('click', '#edit-business-btn', function () {
        editBusiness();
    });

    $('body').on('input', '#name', function () {
        $('#edit-business-btn').removeAttr('disabled')
    });

    $('body').on('input', '#description', function () {
        $('#edit-business-btn').removeAttr('disabled')
    });

    $('body').on('change', '#category_id', function () {
        $('#edit-business-btn').removeAttr('disabled')
    });

};

// places --------------------------------------------------<
window.placeJs = function () {

    let provinceSelected = '';
    $('body').on('click', '#add-province-btn', function () {
        let name = $('#province-name').val()
        $.ajax({
            url: '/admin/place/store',
            type: 'POST',
            dataType: 'html',
            data: {
                name: name,
            },
            success: function (data) {
                if (data.length > 0) {
                    $('#provinces-container').prepend(data);
                    $('#add-province-modal').modal('hide');
                }
            },
            error: (error) => ajaxError(error)
        });

    });

    $('body').on('click', '.show-add-city-modal-btn', function () {
        provinceSelected = $(this).data('province-id');
        $('#add-city-modal').modal('show');
    });

    $('body').on('click', '#add-city-btn', function () {
        let name = $('#city-name').val();
        let province_id = provinceSelected;
        $.ajax({
            url: '/admin/place/add-city',
            type: 'POST',
            dataType: 'html',
            data: {
                name,
                province_id
            },
            success: function (data) {
                if (data.length > 0) {
                    $(`.province${province_id}`).prepend(data);
                    $('#add-city-modal').modal('hide');
                }
            },
            error: (error) => ajaxError(error)
        });

    });

    $('body').on('click', '#add-province-btn', function () {
        let name = $('#province-name').val();
        $.ajax({
            url: '/admin/place/store',
            type: 'POST',
            dataType: 'html',
            data: {
                name: name,
            },
            success: function (data) {
                if (data.length > 0) {
                    $('#provinces-container').prepend(data);
                    $('#add-province-modal').modal('hide');
                }
            },
            error: (error) => ajaxError(error)
        });

    })

};

// settings --------------------------------------------------<
window.settingJs = function () {

    function setFooter(sender) {
        $.ajax({
            url: '/admin/settings/footer',
            type: 'POST',
            dataType: 'json',
            data: $(sender).serialize(),
            success: function (data) {
                if (data.status) {
                    dismisAlert('success', "تنظیمات با موفقیت ذخیره شد.=");
                }
            },
            error: (error) => ajaxError(error)
        });
    }

    $('body').on('submit', '#col-one-form', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        setFooter(this);
    });
    $('body').on('submit', '#col-two-form', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        setFooter(this);
    });
    $('body').on('submit', '#col-three-form', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        setFooter(this);
    });
    $('body').on('submit', '#col-four-form', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        let formData = new FormData();
        let file1 = $('#sign_one')[0].files[0];
        let file2 = $('#sign_two')[0].files[0];
        let file3 = $('#sign_three')[0].files[0];
        if (file1) {
            formData.append('sign1', file1);
        }
        if (file2) {
            formData.append('sign2', file2);
        }
        if (file3) {
            formData.append('sign3', file3);
        }

        formData.append('link1', $('#link1').val());
        formData.append('link2', $('#link2').val());
        formData.append('link3', $('#link3').val());
        formData.append('col', 4);

        $.ajax({
            url: '/admin/settings/footer',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status) {
                    dismisAlert('success', "نشان ها با موفقیت ذخیره شدند")
                } else {
                    dismisAlert('success', "خطا در دخیره نشان ها");
                }
            },
        });
    });

};
