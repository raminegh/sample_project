const loading = $('.loadingio-spinner-eclipse');
let currentThElementSort = null;
let previousThElementSort = null;
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).ready()
    .ajaxStart(function () {
        $('.loadingio-spinner-eclipse').show();
    })
    .ajaxStop(function () {
        $('.loadingio-spinner-eclipse').hide();
    });

const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success mr-2',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
});

const showErrorOccurred = (message = "خطایی رخ داده است") => {
    swalWithBootstrapButtons.fire({
        text: message,
        icon: 'error',
        showConfirmButton: false,
        showCancelButton: true,
        cancelButtonText: 'بستن',
    })
};

const dismisAlert = (type, msg) => {
    Swal.fire({
        position: 'center',
        icon: type,
        title: msg,
        showConfirmButton: false,
        timer: 2000
    });
};

const confirmAlert = (title, text = '', confirmAction) => {
    swalWithBootstrapButtons.fire({
        title: title,
        text: text,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'تایید',
        cancelButtonText: 'انصراف',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            confirmAction();
        }
    })
};

window.parseQuery = function (queryString) {
    let query = {};
    let pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (let i = 0; i < pairs.length; i++) {
        let pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
};

$("body :input").each(function () {
    if ($(this).prop('required')) {
        if ($(this).prev().prop('nodeName') === 'LABEL') {
            $(this).prev().append('<apsn class="text-danger">*</apsn>');
        }
    }
});


/* page function */
window.getNumberPagination = () => {
    return $('ul.pagination').find('li.active').find('.page-link').text();
};

const getPageFromUrl = () => {
    let page = 1;
    const searchParams = new URLSearchParams(window.location.search);
    if (searchParams.has('page')) {
        page = searchParams.get('page');
    }
    if (typeof page === 'string') {
        page = 1;
    }
    return page;
};

let data = {};

const pageLink = () => {
    $('body').on('click', '.page-link', function (e) {
        e.preventDefault();
        data.page = parseQuery($(this).attr('href').split('?')[1]).page;
        changeParamsInUrl("page", data.page);
        load();
    });
};
/* ---- end page function */

/* constant ----- */
const articleLink = "article";
const newsLink = "news";
const discountLink = "discount";
const employmentLink = "employment";
const videoLink = "video";
const imageLink = "image";
const groupLink = "group";
const userLink = "user";
const salonLink = "salon";
const topSalonLink = "salon/top";
const businessLink = "business";
const accessLink = "access";
const commentLink = "comment";
const messageLink = "message";
const accessRoleLink = "access/role";
const accessPermissionLink = "access/permission";
const categoryLink = "category";
const tagLink = "tag";
const adsLink = "ads";

/* ----- end constant */

/* ---- url */
const getParamFromUrl = (key) => {
    let param = null;
    const searchParams = new URLSearchParams(window.location.search);
    if (searchParams.has(key)) {
        param = searchParams.get(key);
    }
    return param;
};

const getUrlParamsObject = () => {
    let params = [], hash;
    let hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (let i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        params.push(hash[0]);
        params[hash[0]] = hash[1];
    }
    return params;
};

const changeParamsInUrl = (key, value) => {
    let searchParams = new URLSearchParams(window.location.search);
    const pathname = window.location.pathname;
    if (value) {
        if (!searchParams.has(key)) {
            searchParams.append(key, value);
        } else {
            searchParams.set(key, value);
        }
    } else {
        if (searchParams.has(key)) {
            searchParams.delete(key);
        }
    }

    const url = pathname + "?" + searchParams.toString();
    window.history.pushState(null, null, url);
};
/* end url ---- */

const getLink = (type = '/list', link = null) => {
    if (!link) {
        const pathname = window.location.pathname;
        if (pathname.includes("article")) {
            link = articleLink;
        } else if (pathname.includes('news')) {
            link = newsLink;
        } else if (pathname.includes('employment')) {
            link = employmentLink;
        } else if (pathname.includes('discount')) {
            link = discountLink;
        } else if (pathname.includes('user')) {
            link = userLink;
        } else if (pathname.includes('salon/top')) {
            link = topSalonLink;
        } else if (pathname.includes('salon')) {
            link = salonLink;
        } else if (pathname.includes('business')) {
            link = businessLink;
        } else if (pathname.includes('video')) {
            link = videoLink;
        } else if (pathname.includes('image')) {
            link = imageLink;
        } else if (pathname.includes('group')) {
            link = groupLink;
        } else if (pathname.includes('access/role')) {
            link = accessRoleLink;
        } else if (pathname.includes('access/permission')) {
            link = accessPermissionLink;
        } else if (pathname.includes('access')) {
            link = accessLink;
        } else if (pathname.includes('comment')) {
            link = commentLink;
        } else if (pathname.includes('message')) {
            link = messageLink;
        } else if (pathname.includes('category')) {
            link = categoryLink;
        } else if (pathname.includes('tag')) {
            link = tagLink;
        } else if (pathname.includes('ads')) {
            link = adsLink;
        }
    }
    if (type === '/list') {
        return '/admin/' + link + type + window.location.search;
    }
    return '/admin/' + link + type;
};

const getCheckedCategory = () => {
    let category = $('.categories-card').find('input:radio[name="category"]:checked').val();
    if (category === undefined) {
        category = getParamFromUrl('category');
        if (category) {
            console.log(`input:radio[value="${category}"]`);
            $('.categories-card').find(`input:radio[value="${category}"]`).prop('checked', true);
        } else {
            $('.categories-card').find(`input:radio[value=""]`).prop('checked', true);
        }
    }
    return category;
};

const categoriesSelectHandler = () => {
    $('.categories-card input:radio[name="category"]').on('change', () => {
        data.category = getCheckedCategory();
        data.page = 1;
        changeParamsInUrl('page', data.page);
        changeParamsInUrl("category", data.category);
        load(data);
    });
};

const ajaxError = (xhr = null, status = null, error = null, message = null) => {
    showErrorOccurred();
};

const load = (targetElement = '.list-container', url = null) => {
    url = url ?? getLink();
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $(targetElement).empty();
            if (data.length > 0) {
                $(targetElement).append(data);
                changeUIAfterSort();
                toggleElement('.filter-container', 'hide');
            } else {
                $(targetElement).append("موردی یافت نشد.");
            }
        },
        error: (xhr, status, error) => ajaxError(xhr, status, error)
    });
};

const deleteFn = (delete_id, target, url = null) => {
    $.ajax({
        url: getLink('/delete', url) + "/" + delete_id,
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                target.slideUp('slow', function () {
                    target.remove();
                    dismisAlert('success', "آیتم موردنظر با موفقیت حذف شد");
                    data.page = getPageFromUrl();
                    load();
                })

            } else {
                showErrorOccurred(data.message);
            }
        },
        error: error => ajaxError(error)
    });
};

const toggleElement = (target, type = 'toggle', speed = 600) => {
    target = $(target);
    if (type === 'show') {
        target.show(speed);
    } else if (type === 'hide') {
        target.hide(speed);
    } else {
        target.toggle(speed);
    }
};

const setDataInElementToUrl = (target) => {
    $(`${target} :input`).each(function () {
        const name = $(this).attr('name');
        const value = $(this).val();
        changeParamsInUrl(name, value);
    });
    load();
};

const searchUser = (type = '') => {
    $('#user').select2({
        placeholder: 'کاربر موردنظر را جستجو و انتخاب کنید',
        dir: "rtl",
        ajax: {
            url: '/admin/user/search/' + type,
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: false
        }
    });
};

const tags = () => {
    $('#tags').select2({
        placeholder: 'جستجو کنید',
        dir: "rtl",
        tags: true,
        maximumSelectionLength: 5,
        ajax: {
            url: '/admin/tags',
            type: "post",
            dataType: 'json',
            delay: 250,
            tokenSeparators: [','],
            data: function (params) {
                return {
                    search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: false

        }
    });
};

const getCategoriesOptions = (type) => {
    $.ajax({
        url: '/admin/category/get-categories-options?type=' + type,
        type: 'get',
        dataType: 'html',
        success: function (data) {
            $('#parent-id').empty();
            $('#parent-id').append(data);
        },
        error: (error) => ajaxError(error)
    });
};

const toggleApprove = (id, $type = '/disapproval', url) => {
    $.ajax({
        url: getLink($type, url) + "/" + id,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data) {
            if (data.status) {
                $('#disapproval-description-modal').modal('hide');
                load();
            } else {
                showErrorOccurred(data.message);
            }
        },
        error: error => ajaxError(error)
    });
};

$('body').on('click', '.delete-btn', function () {
    const id = $(this).data('delete-id');
    const target = $(this).parent().parent();
    const url = $(this).data('delete-url');
    confirmAlert("آیا مطمعن هستید؟", "در صورت تأیید، آیتم موردنظر حذف خواهد شد", () => deleteFn(id, target, url));
});

$('body').on('click', '.disapproval-btn', function () {
    const disapprovalId = $(this).data('disapproval-id');
    $('#disapproval-id').val(disapprovalId);
    $('#disapproval-description-modal').modal('show');
});

$('body').on('click', '.accept-btn', function () {
    const acceptId = $(this).data('accept-id');
    toggleApprove(acceptId, '/accept');
});

$('body').on('click', '.deactive-btn', function () {
    const id = $(this).data('disapproval-id');
    toggleApprove(id, '/disapproval');
});

$('body').on('click', '.disapproval-submit-btn', function () {
    data.disapproval_description = $('#disapproval-description').val();
    const disapprovalId = $('#disapproval-id').val();
    toggleApprove(disapprovalId);
});


$('body').on('click', '#apply-filter-btn', function () {
    setDataInElementToUrl('.filter-container');
});

$('body').on('change', '.filter-date-type-select', function () {
    const dateType = $(this).val();
    if (dateType === 'created_at' || dateType === 'publish_at') {
        toggleElement('.filter-date-container', 'show');
    } else {
        toggleElement('.filter-date-container', 'hide');
    }
});

/* sort ---- */

window.doSort = (sender, sort) => {
    const previousSort = getParamFromUrl('sort');
    const previousType = getParamFromUrl('type');
    let type = 'desc';
    if (previousSort === sort) {
        type = previousType === 'desc' ? 'asc' : 'desc';
    }
    changeParamsInUrl('sort', sort);
    changeParamsInUrl('type', type);
    load();
    previousThElementSort = currentThElementSort;
    currentThElementSort = sender;
};

const changeUIAfterSort = () => {
    if (previousThElementSort) {
        $(previousThElementSort).removeClass('text-primary');
        $(previousThElementSort).find('i').remove();
    }
    if (!$(currentThElementSort).hasClass('text-primary')) {
        $(currentThElementSort).addClass('text-primary');
        const type = getParamFromUrl('type');
        if (type === 'desc') {
            $(currentThElementSort).prepend('<i class="fas fa-sort-up">&nbsp;</i>');
        } else {
            $(currentThElementSort).prepend('<i class="fas fa-sort-down">&nbsp;</i>');
        }
    }
};
/* end sort ---- */

pageLink();
