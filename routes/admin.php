<?php
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'admin.', 'middleware' => ['auth', 'admin', 'root', 'can:admin-root']], function () {

    Route::group(['prefix' => '/profile', 'as' => 'profile.'], function() {

    });

});

Route::group(['as' => 'admin.', 'middleware' => ['auth', 'admin', 'not-unknown-user', 'root', 'can:admin-root']], function () {

    Route::group([], function () {

    });

    Route::group(['prefix' => '/user', 'as' => 'user.'], function () {

    });

    Route::group(['prefix' => '/article', 'as' => 'article.'], function () {
        Route::get('/', 'ArticleController@index')->name('index')->middleware('can:admin-article');
        Route::get('/list', 'ArticleController@list')->name('list')->middleware('can:admin-article-list');
        Route::get('/create', 'ArticleController@create')->name('create')->middleware('can:admin-article-create');
        Route::post('/create', 'ArticleController@store')->name('store')->middleware('can:admin-article-create');
        Route::get('/edit/{article}', 'ArticleController@edit')->name('edit')->middleware('can:admin-article-edit');
        Route::post('/edit/{article}', 'ArticleController@update')->name('update')->middleware('can:admin-article-edit');
        Route::post('/delete/{article}', 'ArticleController@delete')->name('delete')->middleware('can:admin-article-delete');
        Route::post('/disapproval/{article}', 'ArticleController@disapproval')->middleware('can:admin-article-delete');
        Route::post('/accept/{article}', 'ArticleController@accept')->middleware('can:admin-article-delete');

    });

    Route::group(['prefix' => '/news', 'as' => 'news.'], function () {

    });

    Route::group(['prefix' => '/category', 'as' => 'category.'], function () {
        Route::get('/', "CategoryController@index")->name('index')->middleware('can:admin-category');
        Route::get('/list', "CategoryController@list")->name('list')->middleware('can:admin-category-list');
        Route::post('/store', "CategoryController@store")->name('store')->middleware('can:admin-category-create');
        Route::get('/get-categories-options', "CategoryController@getCategories")->middleware('can:admin-category-create');
        Route::post('/update', "CategoryController@update")->name('update')->middleware('can:admin-category-edit');
        Route::post('/delete/{category}', "CategoryController@delete")->name('delete')->middleware('can:admin-category-delete');
    });

    Route::group(['prefix' => '/tag', 'as' => 'tag.'], function () {
    });

    Route::group(['prefix' => '/comment', 'as' => 'comment.'], function () {

    });

    Route::group(['prefix' => '/message', 'as' => 'message.'], function () {

    });

    Route::group(['prefix' => '/group', 'as' => 'group.'], function () {

    });

    Route::group(['prefix' => '/video', 'as' => 'video.'], function () {

    });

    Route::group(['prefix' => '/image', 'as' => 'image.'], function () {

    });

    Route::group(['prefix' => '/service', 'as' => 'service.'], function () {

    });

    Route::group(['prefix' => '/position', 'as' => 'position.'], function () {

    });

    Route::group(['prefix' => '/access', 'as' => 'access.'], function () {

        Route::group(['prefix' => 'role', 'as' => 'role.'], function () {

        });
        Route::group(['prefix' => 'permission', 'as' => 'permission.'], function () {

        });
    });

    Route::group(['prefix' => '/business', 'as' => 'business.'], function () {

    });

    Route::group(['prefix' => '/salon', 'as' => 'salon.'], function () {


    });

    Route::group(['prefix' => '/salon/top', 'as' => 'topSalon.'], function () {

    });

    Route::group(['prefix' => '/place', 'as' => 'place.'], function () {

    });

    Route::group(['prefix' => '/discount', 'as' => 'discount.'], function () {

    });

    Route::group(['prefix' => '/employment', 'as' => 'employment.'], function () {

    });

    Route::group(['prefix' => '/ads', 'as' => 'ads.'], function () {

    });

    Route::group(['prefix' => '/settings', 'as' => 'settings.', 'middleware' => 'can:admin-settings'], function () {
        Route::group(['prefix' => 'home', 'as' => 'home.'], function (){

        });

        Route::group(['prefix' => '/general', 'as' => 'general.'], function (){

        });

        Route::group(['prefix' => '/footer', 'as' => 'footer.'], function (){

        });

        Route::group(['prefix' => '/contact-us', 'as' => 'contact-us.'], function (){

        });
    });

    Route::group(['prefix' => '/statistics', 'as' => 'statistics.'], function () {

    });

});
