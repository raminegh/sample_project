<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'Api'], function (){

    Route::group(['prefix' => '/auth'], function () {

    });

    Route::get('/', 'HomeController@index');


    Route::group(['prefix' => '/profile'], function (){

    });

    Route::group(['prefix' => '/article'], function (){

    });

    Route::group(['prefix' => '/news'], function (){

    });

    Route::group(['prefix' => '/album'], function (){

    });

    Route::group(['prefix' => '/comment'], function (){
    });

    Route::group(['prefix' => '/v', 'as' => 'video.'], function () {

    });

    Route::group(['middleware' => 'auth:api'], function () {

        Route::group(['prefix' => 'dashboard'], function (){
        });

        Route::group(['prefix' => 'user'], function () {


        });

        Route::group(['prefix' => 'business'], function () {

        });

        Route::group(['prefix' => 'position'], function () {

        });

        Route::group(['prefix' => 'service'], function () {
        });

        Route::group(['prefix' => 'salon'], function () {
        });

        Route::group(['prefix' => 'album'], function () {
        });

        Route::group(['prefix' => 'video'], function (){

        });

        Route::group(['prefix' => 'image'], function (){
            Route::post('/upload','ImageController@upload');
        });

        Route::group(['prefix' => 'like'], function (){
            Route::post('/','LikeController@like');
        });

        Route::group(['prefix' => 'tag'], function (){
            Route::get('/','TagController@getList');
        });
    });

});

