<?php
Route::group(['prefix' => '/dashboard'], function () {
    /* Common to all users */
    Route::get('/', 'DashboardController@index')->name('dashboard.index');

    Route::group(['middleware' => 'not-admin'], function () {

    });


    Route::group(['prefix' => 'video'], function () {

        Route::group(['middleware' => 'is-video-owner'], function () {
        });
    });


    Route::group(['prefix' => 'image'], function () {

        Route::group(['middleware' => 'is-image-owner'], function () {
        });
    });

    Route::group(['prefix' => 'ad', 'as' => 'ad.'], function () {
    });

    Route::group(['prefix' => 'message'], function () {

    });

    Route::group(['prefix' => 'album'], function (){

    });

    Route::group(['prefix' => 'employment', 'middleware' => 'can:app-salon'], function () {

    });

    Route::group(['prefix' => 'discount', 'middleware' => 'can:app-salon'], function () {

    });

    Route::group(['prefix' => 'statistics', 'middleware' => 'can:app-salon'], function () {
    });

    Route::group(['prefix' => 'personnel', 'middleware' => 'can:app-salon'], function () {

    });

    Route::group(['prefix' => 'salon', 'middleware' => 'can:app-salon'], function () {
    });
});
