<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'root'], function () {

    Route::group(['namespace' => 'Auth'], function () {
        Route::group(['middleware' => ['guest']], function () {

        });

        Route::get('/logout', 'AuthController@logout')->name('logout')->middleware('auth');
    });

    Route::group(['namespace' => 'Web', 'as' => 'app.'], function () {

        Route::group(['middleware' => ['auth', 'unknown-user']], function () {
            Route::group(['prefix' => 'user', 'as' => 'user.'], function () {

            });
        });

        Route::group(['middleware' => ['auth', 'not-unknown-user']], function () {

            Route::group([], function () {
            });

            require_once('dashboard.php');

            Route::group(['prefix' => 'user', 'as' => 'user.'], function () {

            });

            Route::group(['prefix' => 'business', 'as' => 'business.'], function () {

            });

            Route::group(['prefix' => 'position', 'as' => 'position.'], function () {

            });

            Route::group(['prefix' => 'service'], function () {
            });

            Route::group(['prefix' => 'salon'], function () {
            });

            Route::group(['prefix' => 'album'], function () {

            });


            Route::group(['prefix' => 'like'], function () {
            });

            Route::group(['prefix' => 'tag', 'as' => 'tag.'], function () {

            });

            Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {

            });

            /* send cooperation from salon to beauty activist */
            Route::group(['prefix' => 'cooperation', 'as' => 'cooperation.'], function () {

            });

            Route::group(['prefix' => 'notification', 'as' => 'notification.'], function () {

            });

            Route::group(['prefix' => 'message'], function () {
            });

        });

        Route::get('/', 'HomeController@index')->name('home');

        Route::get('/home', 'HomeController@index')->name('home');

        Route::group(['prefix' => 'user', 'as' => 'user.'], function () {

        });

        Route::group(['prefix' => 'salon', 'as' => 'salon.'], function () {


        });

        Route::group(['prefix' => '/personnel'], function () {
        });

        Route::group(['prefix' => '/profile'], function () {

        });

        Route::group(['prefix' => '/v', 'as' => 'video.'], function () {

        });

        Route::group(['prefix' => '/i', 'as' => 'image.'], function () {

        });

        Route::group(['prefix' => '/album', 'as' => 'album.'], function () {
        });

        Route::group(['prefix' => '/comment'], function () {
        });

        Route::group(['prefix' => '/business', 'as' => 'business.'], function () {
        });

        Route::group(['prefix' => '/service', 'as' => 'service.'], function () {
        });

        Route::group(['prefix' => '/employment', 'as' => 'employment.'], function () {

        });

        Route::group(['prefix' => '/discount', 'as' => 'discount.'], function () {

        });

        Route::group(['prefix' => '/contact-us', 'as' => 'contact-us.'], function () {
        });

        Route::group(['prefix' => '/article', 'as' => 'article.'], function () {

        });

        Route::group(['prefix' => '/news', 'as' => 'news.'], function () {

        });

        Route::group(['prefix' => '/ads', 'as' => 'ads.'], function () {

        });

        Route::group(['prefix' => '/search', 'as' => 'search.'], function () {
        });

        Route::group([], function () {


        });

        Route::get('/{username}', 'ProfileController@profile')->name('profile.username');

    });
});







